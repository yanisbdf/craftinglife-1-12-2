package fr.yanis.craftinglifemod.blocks;

import fr.yanis.craftinglifemod.blocks.tileentities.TECameraB;
import fr.yanis.craftinglifemod.blocks.tileentities.TEOrangeBerryBush;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public class OrangeBerryBush extends Block{
	public OrangeBerryBush(Material material) {
		super(material);
		CraftingLifeBlocks.setBlockName(this, "orangeberrybush");
	}

}
