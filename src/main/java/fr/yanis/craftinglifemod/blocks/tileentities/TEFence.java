package fr.yanis.craftinglifemod.blocks.tileentities;

import com.leviathanstudio.craftstudio.CraftStudioApi;
import com.leviathanstudio.craftstudio.common.animation.AnimationHandler;
import com.leviathanstudio.craftstudio.common.animation.simpleImpl.AnimatedTileEntity;
import fr.yanis.craftinglifemod.CraftingLifeMod;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.world.World;

public class TEFence extends AnimatedTileEntity {

	public static AnimationHandler<TEFence> animHandler = CraftStudioApi.getNewAnimationHandler(TEFence.class);
	public boolean open = true;
	private byte direction;

	static {
		//TileEntityBarrier.animHandler.addAnim(CSCBlocksMod.MODID, "manualbarrier_anim", "manualbarrier", false);
		TEFence.animHandler.addAnim(CraftingLifeMod.MODID, "manualbarrier_anim", "manualbarrier", false);
		TEFence.animHandler.addAnim(CraftingLifeMod.MODID, "manualbarrier_anim_closed", "manualbarrier_anim");
	}

	public TEFence() {
		super();
	}

	public TEFence(World worldIn) {
		this();
		this.world = worldIn;
	}

	@Override
	public AnimationHandler getAnimationHandler() {
		return TEFence.animHandler;
	}

	@Override
	public void update() {
		super.update();
	}

	public byte getDirection() {
		return direction;
	}

	public void setDirection(byte direction) {
		this.direction = direction;
		this.world.markBlockRangeForRenderUpdate(this.pos, this.pos);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		this.direction = nbt.getByte("Direction");
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
		nbt.setByte("Direction", this.direction);
		return super.writeToNBT(nbt);
	}

	@Override
	public SPacketUpdateTileEntity getUpdatePacket() {
		NBTTagCompound nbt = new NBTTagCompound();
		this.writeToNBT(nbt);
		int metadata = getBlockMetadata();
		return new SPacketUpdateTileEntity(this.pos, metadata, nbt);
	}

	@Override
	public NBTTagCompound getUpdateTag() {
		NBTTagCompound nbt = new NBTTagCompound();
		this.writeToNBT(nbt);
		return nbt;
	}

	@Override
	public void handleUpdateTag(NBTTagCompound tag) {
		this.readFromNBT(tag);
	}

	@Override
	public NBTTagCompound getTileData() {
		NBTTagCompound nbt = new NBTTagCompound();
		this.writeToNBT(nbt);
		return nbt;
	}
}
