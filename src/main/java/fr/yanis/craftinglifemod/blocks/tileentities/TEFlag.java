package fr.yanis.craftinglifemod.blocks.tileentities;

import com.leviathanstudio.craftstudio.CraftStudioApi;
import com.leviathanstudio.craftstudio.common.animation.AnimationHandler;
import com.leviathanstudio.craftstudio.common.animation.simpleImpl.AnimatedTileEntity;
import fr.yanis.craftinglifemod.CraftingLifeMod;
import net.minecraft.world.World;

public class TEFlag extends AnimatedTileEntity
{
	protected static AnimationHandler animHandler = CraftStudioApi.getNewAnimationHandler(TEFlag.class);

	static {
		TEFlag.animHandler.addAnim(CraftingLifeMod.MODID, "flag_anim", "flag", true);
	}

	public TEFlag() {
		super();
	}

	public TEFlag(World worldIn) {
		this();
		this.world = worldIn;
	}

	@Override
	public AnimationHandler getAnimationHandler() {
		return TEFlag.animHandler;
	}

	@Override
	public void update() {
		super.update();

		if (this.isWorldRemote() && !this.getAnimationHandler().isAnimationActive(CraftingLifeMod.MODID, "flag_anim", this))
			this.getAnimationHandler().startAnimation(CraftingLifeMod.MODID, "flag_anim", this);

	}

}
