package fr.yanis.craftinglifemod.blocks;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.blocks.tileentities.TECameraB;
import fr.yanis.craftinglifemod.blocks.tileentities.TEFence;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;

public class Fence extends Block implements ITileEntityProvider {

	public Fence(Material materialIn) {
		super(materialIn);
		CraftingLifeBlocks.setBlockName(this, "fence");
	}

	@Nullable
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TEFence();
	}

	@Override
	public boolean hasTileEntity(IBlockState state) {
		return true;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumBlockRenderType getRenderType(IBlockState state) {
		return EnumBlockRenderType.ENTITYBLOCK_ANIMATED;
	}

	@Override
	public boolean isFullCube(IBlockState state) {
		return false;
	}

	@Override
	public boolean isOpaqueCube(IBlockState state) {
		return false;
	}

	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		TileEntity tile = worldIn.getTileEntity(pos);
		if (tile instanceof TEFence) {
			int direction = MathHelper.floor((double) (placer.rotationYaw * 4.0F / 360.0F) + 2.5D) & 3;
			((TEFence) tile).setDirection((byte) direction);
		}
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ){
		TileEntity tile = worldIn.getTileEntity(pos);
		if (tile instanceof TEFence) {
			TEFence te = (TEFence) tile;
			if (worldIn.isRemote && !te.getAnimationHandler().isAnimationActive(CraftingLifeMod.MODID, "manualbarrier_anim_closed", te)
				&& !te.getAnimationHandler().isAnimationActive(CraftingLifeMod.MODID, "manualbarrier_anim", te)) {
				if (te.open) {
					te.getAnimationHandler().stopStartAnimation(CraftingLifeMod.MODID, "manualbarrier_anim_closed", "manualbarrier_anim", 0, te);
					te.open = false;
				} else {
					te.getAnimationHandler().stopStartAnimation(CraftingLifeMod.MODID, "manualbarrier_anim", "manualbarrier_anim_closed", 0, te);
					//te.getAnimationHandler().startAnimation("manualbarrier_anim", 0, te);
					te.open = true;
				}
				return true;
			}
		}
		return false;
	}
}
