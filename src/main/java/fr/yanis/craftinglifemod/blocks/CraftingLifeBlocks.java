package fr.yanis.craftinglifemod.blocks;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class CraftingLifeBlocks {

	public static final Block REDBERRYBUSH = new RedBerryBush(Material.LEAVES).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block VENTILATION = new Ventilation(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block FENCE = new Fence(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block SWITCH = new Switch(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block DESKD = new DeskD(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block COFFEEDISPENSER = new CoffeeDispenser(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block FLAG = new Flag(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block CAMERA = new CameraB(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block LOCKERS = new Lockers(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block COCADISPENSER = new CocaDispenser(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block ELECTRICITYMETER = new ElecticityMeter(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block FUELPUMP = new FuelPump(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block DISPLAY = new Display(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block TRAFFICLIGHT = new TrafficLight(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block FIREEXTINGUISHER = new FireExtinguisher(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block FOOTBALLCAGE = new FootBallCage(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block GARAGE = new Garage(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block GASOLINEJERRYCAN = new GasolineJerrycan(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block GRAPEPRESS = new GrapePress(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block PLAINWARE = new Plainware(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block DRILLMACHINE = new DrillMachine(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block ATM = new ATM(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block ORANGEBERRYBUSH = new OrangeBerryBush(Material.LEAVES).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block SPAWN = new SpawnB(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block FOODSTAND = new FoodStand(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);
	public static final Block GARBAGEBIN = new GarbageBin(Material.ROCK).setCreativeTab(CraftingLifeMod.craftingLifeModBlocksCreativeTabs);


	public static void setBlockName(Block block, String name)
	{
		block.setRegistryName(CraftingLifeMod.MODID, name).setTranslationKey(CraftingLifeMod.MODID + "." + name);
	}
}
