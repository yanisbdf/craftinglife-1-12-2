package fr.yanis.craftinglifemod.blocks;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;

public class RedBerryBush extends Block{

	public RedBerryBush(Material material) {
		super(material);
		CraftingLifeBlocks.setBlockName(this, "redberrybush");
	}

	@Override
	public boolean isOpaqueCube(IBlockState state) {
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state) {
		return false;
	}
}
