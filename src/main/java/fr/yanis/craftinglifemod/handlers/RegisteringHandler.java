package fr.yanis.craftinglifemod.handlers;

import com.flansmod.common.EntityItemCustomRender;
import com.flansmod.common.teams.EntityFlagpole;
import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.blocks.CraftingLifeBlocks;
import fr.yanis.craftinglifemod.client.renderers.RenderDealer;
import fr.yanis.craftinglifemod.entities.EntityDealer;
import fr.yanis.craftinglifemod.entities.EntityItemAdv;
import fr.yanis.craftinglifemod.items.CraftingLifeItems;
import fr.yanis.craftinglifemod.network.PacketConcessVehicList;
import fr.yanis.craftinglifemod.network.PacketDealer;
import fr.yanis.craftinglifemod.network.PacketKillEntity;
import fr.yanis.craftinglifemod.network.PacketVehicleLock;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(modid = CraftingLifeMod.MODID)
@GameRegistry.ObjectHolder(CraftingLifeMod.MODID)
public class RegisteringHandler
{

    @SubscribeEvent
    public void registerItems(RegistryEvent.Register<Item> event) {
    	event.getRegistry().registerAll(CraftingLifeItems.BLOCK_REDBERRYBUSH_ITEM, CraftingLifeItems.BLOCK_VENTILATION_ITEM, CraftingLifeItems.BLOCK_CAMERA_ITEM,
				CraftingLifeItems.BLOCK_COCADISPENSER_ITEM, CraftingLifeItems.BLOCK_COFFEEDISPENSER_ITEM, CraftingLifeItems.BLOCK_DISPLAY_ITEM,
				CraftingLifeItems.BLOCK_DRILLMACHINE_ITEM, CraftingLifeItems.BLOCK_ELECTRICITYMETER_ITEM, CraftingLifeItems.BLOCK_DESKD_ITEM,
				CraftingLifeItems.BLOCK_FIREEXTINGUISHER_ITEM, CraftingLifeItems.BLOCK_FOODSTAND_ITEM, CraftingLifeItems.BLOCK_TRAFFICLIGHT_ITEM, CraftingLifeItems.BLOCK_SWITCH_ITEM,
				CraftingLifeItems.BLOCK_FLAG_ITEM, CraftingLifeItems.BLOCK_SPAWN_ITEM, CraftingLifeItems.BLOCK_PLAINWARE_ITEM, CraftingLifeItems.BLOCK_ATM_ITEM,
				CraftingLifeItems.BLOCK_ORANGEBERRYBUSH_ITEM, CraftingLifeItems.BLOCK_LOCKERS_ITEM, CraftingLifeItems.BLOCK_GASOLINEJERRYCAN_ITEM,
				CraftingLifeItems.BLOCK_GARBAGEBIN_ITEM, CraftingLifeItems.BLOCK_FUELPUMP_ITEM, CraftingLifeItems.BLOCK_FOOTBALLCAGE_ITEM, CraftingLifeItems.BLOCK_GARAGE_ITEM,
				CraftingLifeItems.BLOCK_FENCE_ITEM, CraftingLifeItems.BEER , CraftingLifeItems.BLOCK_GRAPEPRESS_ITEM, CraftingLifeItems.WALLET,
				CraftingLifeItems.REDBULL, CraftingLifeItems.COCACOLA, CraftingLifeItems.COPPERFRAGMENT, CraftingLifeItems.IRONFRAGEMENT, CraftingLifeItems.MOUNTAINDEW, CraftingLifeItems.SPRITE,
			    CraftingLifeItems.WATERBOTTLE, CraftingLifeItems.WRENCH);
    }
    
    @SubscribeEvent
    public void registerBlocks(RegistryEvent.Register<Block> event)
    {
        event.getRegistry().registerAll(CraftingLifeBlocks.REDBERRYBUSH, CraftingLifeBlocks.CAMERA, CraftingLifeBlocks.COCADISPENSER, CraftingLifeBlocks.COFFEEDISPENSER,
				CraftingLifeBlocks.DESKD, CraftingLifeBlocks.DISPLAY, CraftingLifeBlocks.DRILLMACHINE, CraftingLifeBlocks.ELECTRICITYMETER, CraftingLifeBlocks.FENCE,
				CraftingLifeBlocks.FIREEXTINGUISHER, CraftingLifeBlocks.FOODSTAND, CraftingLifeBlocks.FOOTBALLCAGE, CraftingLifeBlocks.FUELPUMP, CraftingLifeBlocks.GARAGE,
				CraftingLifeBlocks.GARBAGEBIN, CraftingLifeBlocks.GASOLINEJERRYCAN, CraftingLifeBlocks.GRAPEPRESS, CraftingLifeBlocks.LOCKERS, CraftingLifeBlocks.ORANGEBERRYBUSH,
				CraftingLifeBlocks.ATM, CraftingLifeBlocks.PLAINWARE, CraftingLifeBlocks.SPAWN, CraftingLifeBlocks.FLAG, CraftingLifeBlocks.SWITCH,
				CraftingLifeBlocks.TRAFFICLIGHT, CraftingLifeBlocks.VENTILATION);
    }

	@SubscribeEvent
	public void registerEntities(RegistryEvent.Register<EntityEntry> event) {

		event.getRegistry().register(new EntityEntry(EntityDealer.class, "Dealer").setRegistryName("Dealer"));
		event.getRegistry().register(new EntityEntry(EntityItemAdv.class, "EntityItemAdv").setRegistryName("EntityItemAdv"));

		EntityRegistry.registerModEntity(new ResourceLocation(CraftingLifeMod.MODID,"Dealer"), EntityDealer.class, "Dealer", 1, CraftingLifeMod.getInstance(),
				40, 1, true);
		EntityRegistry.registerModEntity(new ResourceLocation(CraftingLifeMod.MODID,"EntityItemAdv"), EntityItemAdv.class, "EntityItemAdv", 2, CraftingLifeMod.getInstance(),
			30, 100, true);

	}

	public static void registerMessages(){
    	int id = 0;
		CraftingLifeMod.getPacketHandler().registerMessage(PacketVehicleLock.CommonHandler.class, PacketVehicleLock.class, id++, Side.SERVER);
		CraftingLifeMod.getPacketHandler().registerMessage(PacketKillEntity.CommonHandler.class, PacketKillEntity.class, id++, Side.SERVER);
		CraftingLifeMod.getPacketHandler().registerMessage(PacketDealer.CommonHandler.class, PacketDealer.class, id++, Side.SERVER);
		CraftingLifeMod.getPacketHandler().registerMessage(PacketConcessVehicList.HandlerServer.class, PacketConcessVehicList.class, id++, Side.SERVER);
		CraftingLifeMod.getPacketHandler().registerMessage(PacketConcessVehicList.HandlerClient.class, PacketConcessVehicList.class, id++, Side.CLIENT);
	}

}
