package fr.yanis.craftinglifemod.handlers.gamelogic;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.items.ItemDrink;
import fr.yanis.craftinglifemod.network.PacketThirstStats;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.biome.BiomeDesert;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;

import java.lang.reflect.Field;

/**
	BSD License
	Copyright (c) 2019 www.team-grahou.fr/
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	- Redistributions of source code must retain the above copyright notice, this list of
	conditions and the following disclaimer.
	
	- Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
	the documentation and/or other materials provided with the distribution
	
	- Neither the name of CraftingLife nor the names of its contributors may be used to endorse
	or promote products derived from this software without specific prior written
	permission.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
	SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
	WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
	DAMAGE.
 * 
 * Date: 11/11/2017
 * @author plaigon
 * Main class of Thirst player system, who saves vars to an NBT object, finally going into ExtProp.
 * This class also handles tick logic function (to decrease regarding player exhaustion (from FoodStats class), or to increase).
 */
public class ThirstStats
{
	public int thirstLevel;
	public float saturation;
	public float exhaustion;
	public int thirstTimer;

	public int movementSpeed;

	public transient int lastThirstLevel;
	public transient float lastSaturation;

	public transient DamageSource thirstDmgSource = new DamageThirst();

	public transient Field foodTimer;

	public ThirstStats() {
		lastThirstLevel = -1; // Trigger a refresh when this class is loaded.
		resetStats();
	}

	public void update(EntityPlayer player) {
		if (exhaustion > 5.0f) {
			exhaustion -= 5.0f;
			if (saturation > 0.0f) {
				saturation = Math.max(saturation - 1.0f, 0);
			} else if (player.world.getDifficulty() != EnumDifficulty.PEACEFUL) {
				thirstLevel = Math.max(thirstLevel - 1, 0);
			}
		}

		if (thirstLevel <= 6) {
			player.setSprinting(false);
			if (thirstLevel == 0) {
				if (thirstTimer++ > 200) {
					if (player.getHealth() > 10.0f || player.world.getDifficulty() == EnumDifficulty.HARD || (player.world.getDifficulty() == EnumDifficulty.NORMAL && player.getHealth() > 1.0f)) {
						thirstTimer = 0;
						player.attackEntityFrom(this.thirstDmgSource, 1);
					}
				}
			}
		}

		int ms = player.isRiding() ? 10 : movementSpeed;
		float exhaustMultiplier = player.world.isDaytime() ? 1.0f : 0.9f;
		exhaustMultiplier *= player.world.getBiomeForCoordsBody(player.getPosition()) instanceof BiomeDesert ? 2.0f : 1.0f;

		if (player.isInsideOfMaterial(Material.WATER) || player.isInWater()) {
			addExhaustion(0.03f * ms * 0.003f * exhaustMultiplier);
		} else if (player.onGround) {
			if (player.isSprinting()) {
				addExhaustion(0.06f * ms * 0.018f * exhaustMultiplier);
			} else {
				addExhaustion(0.01f * ms * 0.018f * exhaustMultiplier);
			}
		} else if (!player.isRiding()) { // must be in the air/jumping
			if (player.isSprinting()) {
				addExhaustion(0.06f * ms * 0.025f * exhaustMultiplier);
			} else {
				addExhaustion(0.01f * ms * 0.025f * exhaustMultiplier);
			}
		}

		if (foodTimer == null) {
			try {
				foodTimer = player.getFoodStats().getClass().getDeclaredField("foodTimer");
				foodTimer.setAccessible(true);
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}
		}

		if (thirstLevel < 16) {
			try {
				foodTimer.setInt(player.getFoodStats(), 0);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		// Only send packet update if the thirst level or saturation has changed.
		if (lastThirstLevel != thirstLevel || lastSaturation != saturation) {
			CraftingLifeMod.getPacketHandler().sendTo(new PacketThirstStats(this), (EntityPlayerMP) player);
			lastThirstLevel = thirstLevel;
			lastSaturation = saturation;
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_J)) {
			thirstLevel = Math.max(thirstLevel - 1, 0);
		} else if (Keyboard.isKeyDown(Keyboard.KEY_K)) {
			thirstLevel = Math.min(thirstLevel + 1, 20);
		}
	}

	public void addStats(int heal, float sat) {
		thirstLevel = Math.min(thirstLevel + heal, 20);
		saturation = Math.min(saturation + (heal * sat * 2.0f), thirstLevel);
	}

	public void addExhaustion(float exhaustion) {
		this.exhaustion = Math.min(this.exhaustion + exhaustion, 40.0f);
	}

	public boolean canDrink() {
		return thirstLevel < 20;
	}

	public int getMovementSpeed(EntityPlayer player) {
		double x = player.posX - player.prevPosX;
		double y = player.posY - player.prevPosY;
		double z = player.posZ - player.prevPosZ;
		return (int) Math.round(100.0d * Math.sqrt(x*x + y*y + z*z));
	}

	public void resetStats() {
		thirstLevel = 20;
		saturation = 5f;
		exhaustion = 0f;
	}

	public static class DamageThirst extends DamageSource {
		public DamageThirst() {
			super("thirst");
			setDamageBypassesArmor();
			setDamageIsAbsolute();
		}

		@Override
		public ITextComponent getDeathMessage(EntityLivingBase entity) {
			if(entity instanceof EntityPlayer) {
				EntityPlayer player = (EntityPlayer)entity;
				return new TextComponentString(player.getDisplayName() + "'s body is now made up of 0% water!");
			}
			return super.getDeathMessage(entity);
		}
	}
}
