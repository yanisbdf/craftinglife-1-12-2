package fr.yanis.craftinglifemod.entities;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.client.gui.GuiConcessionnaire;
import fr.yanis.craftinglifemod.client.gui.GuiNPC;
import fr.yanis.craftinglifemod.items.CraftingLifeItems;
import fr.yanis.craftinglifemod.network.PacketConcessVehicList;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class EntityDealer extends EntityLiving {
	public Map<String, Integer> vehicules = new HashMap();
	public static final DataParameter<Integer> skin = EntityDataManager.createKey(EntityDealer.class, DataSerializers.VARINT);

	private void sendOpenGuiMessage(EntityPlayer to) {

		NBTTagCompound data = new NBTTagCompound();
		this.writeVehiculesDataToNBT(data);
		data.setInteger("Action", 2);
		data.setInteger("EntId", getEntityId());
		CraftingLifeMod.getPacketHandler().sendTo(new PacketConcessVehicList(data), (EntityPlayerMP) to);
	}

	private void sendOpenSetupGuiMessage(EntityPlayer to) {
		NBTTagCompound data = new NBTTagCompound();
		this.writeVehiculesDataToNBT(data);
		data.setInteger("Action", 3);
		data.setInteger("EntId", getEntityId());
		CraftingLifeMod.getPacketHandler().sendTo(new PacketConcessVehicList(data), (EntityPlayerMP) to);
	}

	public String concessName = "";

	public EntityDealer(World world) {
		super(world);
	}

	public EntityDealer(World world, double x, double y, double z) {
		super(world);
		setCustomNameTag("Vendeur de vehicule");
		setAlwaysRenderNameTag(true);
		setPosition(x, y, z);
	}

	@Override
	protected void entityInit() {
		super.entityInit();
		dataManager.register(skin, 0);
	}


	@Override
	public void writeEntityToNBT(NBTTagCompound compound) {
		super.writeEntityToNBT(compound);
		compound.setInteger("Skin", getSkin());
		this.writeVehiculesDataToNBT(compound);
	}

	private void writeVehiculesDataToNBT(NBTTagCompound compound) {
		compound.setString("Concessionnaire", concessName);

		NBTTagList tagList = new NBTTagList();
		for (Entry<String, Integer> ent : vehicules.entrySet()) {
			NBTTagCompound tag = new NBTTagCompound();
			tag.setString("name", ent.getKey());
			tag.setInteger("price", ent.getValue());
			tagList.appendTag(tag);
		}
		compound.setTag("VehiculeList", tagList);
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound compound) {
		super.readEntityFromNBT(compound);
		setSkin(compound.getInteger("Entity"));
		readVehiculesDataFromNBT(compound);
	}

	public void readVehiculesDataFromNBT(NBTTagCompound compound) {
		concessName = compound.getString("Concessionnaire");

		vehicules.clear();
		NBTTagList tagList = compound.getTagList("VehiculeList", Constants.NBT.TAG_COMPOUND);
		for (int i = 0; i < tagList.tagCount(); i++) {
			NBTTagCompound tag = tagList.getCompoundTagAt(i);
			vehicules.put(tag.getString("name"), tag.getInteger("price"));
		}
	}

	public void onLivingUpdate() {

	}

	@Override
	protected boolean processInteract(EntityPlayer player, EnumHand hand) {

		if (player.isSneaking() && player.getHeldItem(EnumHand.MAIN_HAND) != null && player.getHeldItem(EnumHand.MAIN_HAND).getItem().equals(CraftingLifeItems.WRENCH)) {
			sendOpenSetupGuiMessage(player);
		} else {
			if (concessName.isEmpty() || vehicules.isEmpty())
				player.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + "§cCe concessionnaire n'est pas disponible"));
			else
				sendOpenGuiMessage(player);
		}

		return false;
	}

	@SideOnly(Side.CLIENT)
	public void guiDisplaySetup(EntityPlayer entityplayer) {
		Minecraft.getMinecraft().displayGuiScreen(new GuiNPC(entityplayer, this));
	}

	@SideOnly(Side.CLIENT)
	public void guiDisplayConcessionnaire(EntityPlayer entityplayer) {
		Minecraft.getMinecraft().displayGuiScreen(new GuiConcessionnaire(this, entityplayer));
	}

	public int getSkin() {
		return dataManager.get(skin);
	}

	public void setSkin(int id) {
		dataManager.set(skin, id);
	}

	@Override
	public boolean attackEntityFrom(DamageSource damagesource, float i) {
		return false;
	}

}
