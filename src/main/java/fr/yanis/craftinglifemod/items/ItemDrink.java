package fr.yanis.craftinglifemod.items;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.handlers.gamelogic.ThirstStats;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemDrink extends Item
{
	public ItemDrink(ItemDrink itemDrink, String unlocalisedName) {
		CraftingLifeItems.setItemName(itemDrink, unlocalisedName);
		this.setCreativeTab(CreativeTabs.FOOD);
		this.setHasSubtypes(true);
	}

	@Override
	public int getMaxItemUseDuration(ItemStack stack) {
		return 1;
	}

	@Override
	public EnumAction getItemUseAction(ItemStack stack) {
		return EnumAction.DRINK;
	}

	/*public void onDrinkItem(EntityPlayer player, ItemStack stack) {
		if (!player.world.isRemote && player != null) {
			ThirstStats stats = ThirstMod.getProxy().getStatsByUUID(player.getUniqueID());
			Drink drink = getDrinkFromMetadata(stack.getMetadata());
			stats.addStats(drink.thirstReplenish, drink.saturationReplenish);
			player.addStat(StatList.getObjectUseStats(this));
		}
	}*/
}
