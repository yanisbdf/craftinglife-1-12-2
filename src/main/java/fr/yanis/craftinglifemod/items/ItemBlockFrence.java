package fr.yanis.craftinglifemod.items;

import fr.yanis.craftinglifemod.blocks.CraftingLifeBlocks;
import fr.yanis.craftinglifemod.blocks.tileentities.TEFence;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class ItemBlockFrence extends ItemBlock {
	public ItemBlockFrence(Block fence) {
		super(fence);
	}

	public EnumActionResult onItemUse(EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		int direction = MathHelper.floor((double) (playerIn.rotationYaw * 4.0F / 360.0F) + 2.5D) & 3;
		System.out.println(direction);
		switch (direction) {
			case 0:
				multiBlockX(playerIn.getHeldItem(hand), playerIn, worldIn, pos, facing);
				break;
			case 1:
				multiBlockZ(playerIn.getHeldItem(hand), playerIn, worldIn, pos, facing);
				break;
			case 2:
				multiBlockXNegtive(playerIn.getHeldItem(hand), playerIn, worldIn, pos, facing);
				break;
			case 3:
				multiBlockZ(playerIn.getHeldItem(hand), playerIn, worldIn, pos, facing);
				break;
		}
		TileEntity tile = worldIn.getTileEntity(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ()));
		if (tile instanceof TEFence) {
			((TEFence) tile).setDirection((byte) direction);
		}
		return EnumActionResult.SUCCESS;


	}

	private void multiBlockXNegtive(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side) {
		if (playerIn.canPlayerEdit(pos, side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() - 2, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 1, pos.getY() + 1, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX() + 2, pos.getY() + 1, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 3, pos.getY() + 1, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX() + 4, pos.getY() + 1, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 5, pos.getY() + 1, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX() + 6, pos.getY() + 1, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 7, pos.getY() + 1, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 1, pos.getY() + 2, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 3, pos.getY() + 2, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX() + 4, pos.getY() + 2, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 5, pos.getY() + 2, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX() + 6, pos.getY() + 2, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 7, pos.getY() + 2, pos.getZ()), side, stack)) {
			Block i1 = worldIn.getBlockState(pos).getBlock();
			boolean isReplaceable = (i1 == Blocks.VINE || i1 == Blocks.TALLGRASS || i1 == Blocks.DEADBUSH || (i1 != null & i1.isReplaceable(worldIn, pos)));
			if (worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 1, pos.getY() + 1, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 2, pos.getY() + 1, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 3, pos.getY() + 1, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 4, pos.getY() + 1, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 5, pos.getY() + 1, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 6, pos.getY() + 1, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 7, pos.getY() + 1, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 1, pos.getY() + 2, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 2, pos.getY() + 2, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 3, pos.getY() + 2, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 4, pos.getY() + 2, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 5, pos.getY() + 2, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 6, pos.getY() + 2, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 7, pos.getY() + 2, pos.getZ()))) {
				worldIn.setBlockState(new BlockPos(pos.getX() + 1, pos.getY() - 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() - 1, pos.getZ()), CraftingLifeBlocks.FENCE.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 1, pos.getY() - 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 2, pos.getY() - 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 3, pos.getY() - 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 4, pos.getY() - 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 5, pos.getY() - 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 6, pos.getY() - 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 7, pos.getY() - 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 1, pos.getY() - 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() - 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 1, pos.getY() - 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 2, pos.getY() - 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 3, pos.getY() - 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 4, pos.getY() - 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 5, pos.getY() - 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 6, pos.getY() - 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 7, pos.getY() - 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
			}
		}

	}

	private void multiBlockX(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side) {
		if (playerIn.canPlayerEdit(pos, side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 1, pos.getY() + 1, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX() + 2, pos.getY() + 1, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 3, pos.getY() + 1, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX() + 4, pos.getY() + 1, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 5, pos.getY() + 1, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX() + 6, pos.getY() + 1, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 7, pos.getY() + 1, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 1, pos.getY() + 2, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 3, pos.getY() + 2, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX() + 4, pos.getY() + 2, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 5, pos.getY() + 2, pos.getZ()), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX() + 6, pos.getY() + 2, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX() + 7, pos.getY() + 2, pos.getZ()), side, stack)) {
			Block i1 = worldIn.getBlockState(pos).getBlock();
			boolean isReplaceable = (i1 == Blocks.VINE || i1 == Blocks.TALLGRASS || i1 == Blocks.DEADBUSH || (i1 != null & i1.isReplaceable(worldIn, pos)));
			if (worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 1, pos.getY() + 1, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 2, pos.getY() + 1, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 3, pos.getY() + 1, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 4, pos.getY() + 1, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 5, pos.getY() + 1, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 6, pos.getY() + 1, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 7, pos.getY() + 1, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 1, pos.getY() + 2, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 2, pos.getY() + 2, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 3, pos.getY() + 2, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 4, pos.getY() + 2, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 5, pos.getY() + 2, pos.getZ())) && worldIn.isAirBlock(new BlockPos(pos.getX() + 6, pos.getY() + 2, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX() + 7, pos.getY() + 2, pos.getZ()))) {
				worldIn.setBlockState(new BlockPos(pos.getX() - 1, pos.getY() + 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ()), CraftingLifeBlocks.FENCE.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 1, pos.getY() + 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 2, pos.getY() + 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 3, pos.getY() + 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 4, pos.getY() + 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 5, pos.getY() + 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 6, pos.getY() + 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 7, pos.getY() + 1, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() - 1, pos.getY() + 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 1, pos.getY() + 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 2, pos.getY() + 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 3, pos.getY() + 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 4, pos.getY() + 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 5, pos.getY() + 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 6, pos.getY() + 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX() + 7, pos.getY() + 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
			}
		}

	}

	private void multiBlockZ(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side) {
		if (playerIn.canPlayerEdit(pos, side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ()), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 1), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 2), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 3), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 4), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 5), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 6), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 7), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 1), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 2), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 3), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 4), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 5), side, stack) && playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 6), side, stack)
			&& playerIn.canPlayerEdit(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 7), side, stack)) {
			Block i1 = worldIn.getBlockState(pos).getBlock();
			boolean isReplaceable = (i1 == Blocks.VINE || i1 == Blocks.TALLGRASS || i1 == Blocks.DEADBUSH || (i1 != null & i1.isReplaceable(worldIn, pos)));
			if (worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ()))
				&& worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 1)) && worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 2))
				&& worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 3)) && worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 4))
				&& worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 5)) && worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 6))
				&& worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 7))
				&& worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 1)) && worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 2))
				&& worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 3)) && worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 4))
				&& worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 5)) && worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 6))
				&& worldIn.isAirBlock(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 7))) {
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + -1), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ()), CraftingLifeBlocks.FENCE.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 1), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 2), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 3), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 4), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 5), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 6), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 7), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() - 1), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ()), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 1), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 2), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 3), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 4), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 5), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 6), Blocks.BARRIER.getDefaultState(), 3);
				worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY() + 2, pos.getZ() + 7), Blocks.BARRIER.getDefaultState(), 3);

			}
		}
	}
}
