package fr.yanis.craftinglifemod.items;

import net.minecraft.item.Item;

public class Wallet extends Item {

	public Wallet() {
		super();
		CraftingLifeItems.setItemName(this, "wallet");
	}
}
