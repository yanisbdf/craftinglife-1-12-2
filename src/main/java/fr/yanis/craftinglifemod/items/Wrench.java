package fr.yanis.craftinglifemod.items;

import com.flansmod.common.driveables.EntityVehicle;
import fr.yanis.craftinglifemod.entities.EntityDealer;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class Wrench extends Item {

	public Wrench() {
		super();
		CraftingLifeItems.setItemName(this, "wrench");
	}

	public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer entityplayer, EnumHand hand) {
		ItemStack itemstack = entityplayer.getHeldItem(hand);

		//Raytracing
		float cosYaw = MathHelper.cos(-entityplayer.rotationYaw * 0.01745329F - 3.141593F);
		float sinYaw = MathHelper.sin(-entityplayer.rotationYaw * 0.01745329F - 3.141593F);
		float cosPitch = -MathHelper.cos(-entityplayer.rotationPitch * 0.01745329F);
		float sinPitch = MathHelper.sin(-entityplayer.rotationPitch * 0.01745329F);
		double length = 5D;
		Vec3d posVec = new Vec3d(entityplayer.posX, entityplayer.posY + 1.62D - entityplayer.getYOffset(), entityplayer.posZ);
		Vec3d lookVec = posVec.add(sinYaw * cosPitch * length, sinPitch * length, cosYaw * cosPitch * length);
		RayTraceResult RayTraceResult = world.rayTraceBlocks(posVec, lookVec, false);

		//Result check
		if (RayTraceResult == null) {
			return new ActionResult<>(EnumActionResult.PASS, itemstack);
		}
		if (RayTraceResult.typeOfHit == net.minecraft.util.math.RayTraceResult.Type.BLOCK) {
			BlockPos pos = RayTraceResult.getBlockPos();
				if (!world.isRemote) {
					if (entityplayer.isSneaking()) {
						if (entityplayer.getHeldItem(hand).getItem().equals(CraftingLifeItems.WRENCH)) {
							EntityDealer vehicle = new EntityDealer(world, (double) pos.getX() + 0.5F, (double) pos.getY()+ 1F, (double) pos.getZ() + 0.5F);
							world.spawnEntity(vehicle);
						}
					}
				}

		}
		return new ActionResult<>(EnumActionResult.PASS, entityplayer.getHeldItem(hand));
	}

}
