package fr.yanis.craftinglifemod.items.drink;

import fr.yanis.craftinglifemod.items.CraftingLifeItems;
import net.minecraft.item.Item;

public class Cocacola extends Item {

	public Cocacola() {
		super();
		CraftingLifeItems.setItemName(this, "cocacola");
	}

}
