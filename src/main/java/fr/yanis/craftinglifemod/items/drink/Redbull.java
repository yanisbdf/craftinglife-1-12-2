package fr.yanis.craftinglifemod.items.drink;

import fr.yanis.craftinglifemod.items.CraftingLifeItems;
import net.minecraft.item.Item;

public class Redbull extends Item {

	public Redbull() {
		super();
		CraftingLifeItems.setItemName(this, "redbull");
	}

}
