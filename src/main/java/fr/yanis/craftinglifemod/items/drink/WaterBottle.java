package fr.yanis.craftinglifemod.items.drink;

import fr.yanis.craftinglifemod.items.CraftingLifeItems;
import net.minecraft.item.Item;

public class WaterBottle extends Item {

	public WaterBottle() {
		super();
		CraftingLifeItems.setItemName(this, "waterbottle");
	}

}
