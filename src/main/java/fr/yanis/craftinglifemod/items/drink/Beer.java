package fr.yanis.craftinglifemod.items.drink;

import fr.yanis.craftinglifemod.items.CraftingLifeItems;
import fr.yanis.craftinglifemod.items.ItemDrink;
import net.minecraft.item.Item;

public class Beer extends Item {

	public Beer() {
		super();
		CraftingLifeItems.setItemName(this, "beer");
	}

}
