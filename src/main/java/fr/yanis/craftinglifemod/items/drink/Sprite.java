package fr.yanis.craftinglifemod.items.drink;

import fr.yanis.craftinglifemod.items.CraftingLifeItems;
import net.minecraft.item.Item;

public class Sprite extends Item {

	public Sprite() {
		super();
		CraftingLifeItems.setItemName(this, "sprite");
	}

}
