package fr.yanis.craftinglifemod.items.drink;

import fr.yanis.craftinglifemod.items.CraftingLifeItems;
import net.minecraft.item.Item;

public class MountainDew extends Item {

	public MountainDew() {
		super();
		CraftingLifeItems.setItemName(this, "mountaindew");
	}

}
