package fr.yanis.craftinglifemod.items;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.blocks.*;
import fr.yanis.craftinglifemod.items.drink.*;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;

import static net.minecraftforge.common.EnumPlantType.Water;

@Mod.EventBusSubscriber(value = Side.CLIENT, modid = CraftingLifeMod.MODID)
public class CraftingLifeItems {

	//ITEMBLOCKS
	public static final Item BLOCK_REDBERRYBUSH_ITEM = new ItemBlock(CraftingLifeBlocks.REDBERRYBUSH).setRegistryName(CraftingLifeBlocks.REDBERRYBUSH.getRegistryName());
	public static final Item BLOCK_VENTILATION_ITEM = new ItemBlock(CraftingLifeBlocks.VENTILATION).setRegistryName(CraftingLifeBlocks.VENTILATION.getRegistryName());
	public static final Item BLOCK_FENCE_ITEM = new ItemBlockFrence(CraftingLifeBlocks.FENCE).setRegistryName(CraftingLifeBlocks.FENCE.getRegistryName());
	public static final Item BLOCK_SWITCH_ITEM = new ItemBlock(CraftingLifeBlocks.SWITCH).setRegistryName(CraftingLifeBlocks.SWITCH.getRegistryName());
	public static final Item BLOCK_DESKD_ITEM = new ItemBlock(CraftingLifeBlocks.DESKD).setRegistryName(CraftingLifeBlocks.DESKD.getRegistryName());
	public static final Item BLOCK_COFFEEDISPENSER_ITEM = new ItemBlock(CraftingLifeBlocks.COFFEEDISPENSER).setRegistryName(CraftingLifeBlocks.COFFEEDISPENSER.getRegistryName());
	public static final Item BLOCK_FLAG_ITEM = new ItemBlock(CraftingLifeBlocks.FLAG).setRegistryName(CraftingLifeBlocks.FLAG.getRegistryName());
	public static final Item BLOCK_CAMERA_ITEM = new ItemBlock(CraftingLifeBlocks.CAMERA).setRegistryName(CraftingLifeBlocks.CAMERA.getRegistryName());
	public static final Item BLOCK_LOCKERS_ITEM = new ItemBlock(CraftingLifeBlocks.LOCKERS).setRegistryName(CraftingLifeBlocks.LOCKERS.getRegistryName());
	public static final Item BLOCK_COCADISPENSER_ITEM = new ItemBlock(CraftingLifeBlocks.COCADISPENSER).setRegistryName(CraftingLifeBlocks.COCADISPENSER.getRegistryName());
	public static final Item BLOCK_ELECTRICITYMETER_ITEM = new ItemBlock(CraftingLifeBlocks.ELECTRICITYMETER).setRegistryName(CraftingLifeBlocks.ELECTRICITYMETER.getRegistryName());
	public static final Item BLOCK_FUELPUMP_ITEM = new ItemBlock(CraftingLifeBlocks.FUELPUMP).setRegistryName(CraftingLifeBlocks.FUELPUMP.getRegistryName());
	public static final Item BLOCK_DISPLAY_ITEM = new ItemBlock(CraftingLifeBlocks.DISPLAY).setRegistryName(CraftingLifeBlocks.DISPLAY.getRegistryName());
	public static final Item BLOCK_TRAFFICLIGHT_ITEM = new ItemBlock(CraftingLifeBlocks.TRAFFICLIGHT).setRegistryName(CraftingLifeBlocks.TRAFFICLIGHT.getRegistryName());
	public static final Item BLOCK_FIREEXTINGUISHER_ITEM = new ItemBlock(CraftingLifeBlocks.FIREEXTINGUISHER).setRegistryName(CraftingLifeBlocks.FIREEXTINGUISHER.getRegistryName());
	public static final Item BLOCK_FOOTBALLCAGE_ITEM = new ItemBlock(CraftingLifeBlocks.FOOTBALLCAGE).setRegistryName(CraftingLifeBlocks.FOOTBALLCAGE.getRegistryName());
	public static final Item BLOCK_GARAGE_ITEM = new ItemBlock(CraftingLifeBlocks.GARAGE).setRegistryName(CraftingLifeBlocks.GARAGE.getRegistryName());
	public static final Item BLOCK_GASOLINEJERRYCAN_ITEM = new ItemBlock(CraftingLifeBlocks.GASOLINEJERRYCAN).setRegistryName(CraftingLifeBlocks.GASOLINEJERRYCAN.getRegistryName());
	public static final Item BLOCK_GRAPEPRESS_ITEM = new ItemBlock(CraftingLifeBlocks.GRAPEPRESS).setRegistryName(CraftingLifeBlocks.GRAPEPRESS.getRegistryName());
	public static final Item BLOCK_PLAINWARE_ITEM = new ItemBlock(CraftingLifeBlocks.PLAINWARE).setRegistryName(CraftingLifeBlocks.PLAINWARE.getRegistryName());
	public static final Item BLOCK_DRILLMACHINE_ITEM = new ItemBlock(CraftingLifeBlocks.DRILLMACHINE).setRegistryName(CraftingLifeBlocks.DRILLMACHINE.getRegistryName());
	public static final Item BLOCK_ATM_ITEM = new ItemBlock(CraftingLifeBlocks.ATM).setRegistryName(CraftingLifeBlocks.ATM.getRegistryName());
	public static final Item BLOCK_ORANGEBERRYBUSH_ITEM = new ItemBlock(CraftingLifeBlocks.ORANGEBERRYBUSH).setRegistryName(CraftingLifeBlocks.ORANGEBERRYBUSH.getRegistryName());
	public static final Item BLOCK_SPAWN_ITEM = new ItemBlock(CraftingLifeBlocks.SPAWN).setRegistryName(CraftingLifeBlocks.SPAWN.getRegistryName());
	public static final Item BLOCK_FOODSTAND_ITEM = new ItemBlock(CraftingLifeBlocks.FOODSTAND).setRegistryName(CraftingLifeBlocks.FOODSTAND.getRegistryName());
	public static final Item BLOCK_GARBAGEBIN_ITEM = new ItemBlock(CraftingLifeBlocks.GARBAGEBIN).setRegistryName(CraftingLifeBlocks.GARBAGEBIN.getRegistryName());

	//ITEMS
	public static final Item COPPERFRAGMENT = new CopperFragment().setCreativeTab(CraftingLifeMod.craftingLifeModItemsCreativeTabs);
	public static final Item IRONFRAGEMENT = new IronFragment().setCreativeTab(CraftingLifeMod.craftingLifeModItemsCreativeTabs);
	public static final Item WALLET = new Wallet().setCreativeTab(CraftingLifeMod.craftingLifeModItemsCreativeTabs);
	public static final Item WRENCH = new Wrench().setCreativeTab(CraftingLifeMod.craftingLifeModItemsCreativeTabs);

	//FOOD
	public static final Item BEER = new Beer().setCreativeTab(CraftingLifeMod.craftingLifeModItemsCreativeTabs);
	public static final Item COCACOLA = new Cocacola().setCreativeTab(CraftingLifeMod.craftingLifeModItemsCreativeTabs);
	public static final Item SPRITE = new Sprite().setCreativeTab(CraftingLifeMod.craftingLifeModItemsCreativeTabs);
	public static final Item MOUNTAINDEW = new MountainDew().setCreativeTab(CraftingLifeMod.craftingLifeModItemsCreativeTabs);
	public static final Item REDBULL = new Redbull().setCreativeTab(CraftingLifeMod.craftingLifeModItemsCreativeTabs);
	public static final Item WATERBOTTLE = new WaterBottle().setCreativeTab(CraftingLifeMod.craftingLifeModItemsCreativeTabs);

	/*public static final Item BEER = new Beer();
	public static final Item COCACOLA = new Cocacola();
	public static final Item SPRITE = new Sprite();
	public static final Item MOUNTAINDEW = new MountainDew();
	public static final Item REDBULL = new Redbull();
	public static final Item WATER = new Water();
	public static final Item BEER = new Beer();
	public static final Item COCACOLA = new Cocacola();
	public static final Item SPRITE = new Sprite();
	public static final Item MOUNTAINDEW = new MountainDew();
	public static final Item REDBULL = new Redbull();
	public static final Item WATER = new Water();*/

	//public static final Item ITEM_PHONE = new Phone();

	public static void setItemName(Item item, String name)
	{
		item.setRegistryName(CraftingLifeMod.MODID, name).setTranslationKey(CraftingLifeMod.MODID + "." + name);
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public static void registerItemModels(ModelRegistryEvent event)
	{
		registerModel(BLOCK_REDBERRYBUSH_ITEM, 0);
		registerModel(BLOCK_VENTILATION_ITEM, 0);
		registerModel(BLOCK_FENCE_ITEM, 0);
		registerModel(BLOCK_SWITCH_ITEM, 0);
		registerModel(BLOCK_DESKD_ITEM, 0);
		registerModel(BLOCK_COFFEEDISPENSER_ITEM, 0);
		registerModel(BLOCK_FLAG_ITEM, 0);
		registerModel(BLOCK_CAMERA_ITEM, 0);
		registerModel(BLOCK_LOCKERS_ITEM, 0);
		registerModel(BLOCK_COCADISPENSER_ITEM, 0);
		registerModel(BLOCK_ELECTRICITYMETER_ITEM, 0);
		registerModel(BLOCK_FUELPUMP_ITEM, 0);
		registerModel(BLOCK_DISPLAY_ITEM, 0);
		registerModel(BLOCK_FIREEXTINGUISHER_ITEM, 0);
		registerModel(BLOCK_FOOTBALLCAGE_ITEM, 0);
		registerModel(BLOCK_GARAGE_ITEM, 0);
		registerModel(BLOCK_GASOLINEJERRYCAN_ITEM, 0);
		registerModel(BLOCK_TRAFFICLIGHT_ITEM, 0);
		registerModel(BLOCK_GRAPEPRESS_ITEM, 0);
		registerModel(BLOCK_PLAINWARE_ITEM, 0);
		registerModel(BLOCK_DRILLMACHINE_ITEM, 0);
		registerModel(BLOCK_ATM_ITEM, 0);
		registerModel(BLOCK_ORANGEBERRYBUSH_ITEM, 0);
		registerModel(BLOCK_SPAWN_ITEM, 0);
		registerModel(BLOCK_FOODSTAND_ITEM, 0);
		registerModel(BLOCK_GARBAGEBIN_ITEM, 0);

		registerModel(COPPERFRAGMENT, 0);
		registerModel(IRONFRAGEMENT, 0);
		registerModel(WALLET, 0);
		registerModel(WRENCH, 0);

		registerModel(BEER, 0);
		registerModel(COCACOLA, 0);
		registerModel(SPRITE, 0);
		registerModel(MOUNTAINDEW, 0);
		registerModel(REDBULL, 0);
		registerModel(WATERBOTTLE, 0);

		//registerModel(ITEM_PHONE, 0);
	}

	@SideOnly(Side.CLIENT)
	public static void registerModel(Item item, int metadata)
	{
		if(metadata < 0)
			metadata = 0;
		String resourceName = item.getTranslationKey().substring(5).replace('.', ':');
		if(metadata > 0)
			resourceName += "_m" + metadata;

		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(resourceName, "inventory"));
	}
}
