package fr.yanis.craftinglifemod.items;

import net.minecraft.item.Item;

public class CopperFragment extends Item {

	public CopperFragment() {
		super();
		CraftingLifeItems.setItemName(this, "copperfragment");
	}
}
