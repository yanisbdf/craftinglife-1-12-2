package fr.yanis.craftinglifemod.common;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.handlers.gamelogic.ThirstStats;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.NetworkManager;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.io.*;

public class CommonEventHandler {

	/*@SubscribeEvent
	public void onPlayerTick(TickEvent.PlayerTickEvent event) {
		if(!event.player.world.isRemote) {
			ThirstStats stats = CraftingLifeMod.proxy.getStatsByUUID(event.player.getUniqueID());
			if(stats != null) {
				stats.update(event.player);
			}
		} else {
			CraftingLifeMod.getPacketHandler().sendToServer(new PacketMovementSpeed(event.player, ThirstMod.getClientProxy().clientStats));
		}
	}

	@SubscribeEvent
	public void onAttack(AttackEntityEvent attack) {
		if (!attack.getEntityPlayer().world.isRemote) {
			ThirstStats stats = CraftingLifeMod.proxy.getStatsByUUID(attack.getEntityPlayer().getUniqueID());
			stats.addExhaustion(0.5f);
		}
		attack.setResult(Event.Result.DEFAULT);
	}

	@SubscribeEvent
	public void onHurt(LivingHurtEvent hurt) {
		if (hurt.getEntity() instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) hurt.getEntity();
			if (!player.world.isRemote) {
				ThirstStats stats = CraftingLifeMod.proxy.getStatsByUUID(player.getUniqueID());
				stats.addExhaustion(0.4f);
			}
		}
		hurt.setResult(Event.Result.DEFAULT);
	}

	@SubscribeEvent
	public void onBlockBreak(BlockEvent.BreakEvent event) {
		EntityPlayer player = event.getPlayer();
		if(player != null) {
			if(!player.world.isRemote) {
				ThirstStats stats = CraftingLifeMod.proxy.getStatsByUUID(player.getUniqueID());
				stats.addExhaustion(0.03f);
			}
		}
		event.setResult(Event.Result.DEFAULT);
	}

	public void playedCloned(net.minecraftforge.event.entity.player.PlayerEvent.Clone event) {
		if(!event.getEntityPlayer().world.isRemote) {
			if(event.isWasDeath()) {
				ThirstStats stats = CraftingLifeMod.proxy.getStatsByUUID(event.getEntityPlayer().getUniqueID());
				stats.resetStats();
			}
		}
	}

	@SubscribeEvent
	public void onLoadPlayerData(PlayerEvent.LoadFromFile event) {
		if (!event.getEntityPlayer().world.isRemote) {
			EntityPlayer player = event.getEntityPlayer();
			File saveFile = event.getPlayerFile("craftinglifethirstmod");
			if(!saveFile.exists()) {
				CraftingLifeMod.proxy.registerPlayer(player, new ThirstStats());
			} else {
				try {
					FileReader reader = new FileReader(saveFile);
					ThirstStats stats = CraftingLifeMod.gsonInstance.fromJson(reader, ThirstStats.class);
					if (stats == null) {
						CraftingLifeMod.proxy.registerPlayer(player, new ThirstStats());
					} else {
						CraftingLifeMod.proxy.registerPlayer(player, stats);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@SubscribeEvent
	public void onSavePlayerData(PlayerEvent.SaveToFile event) {
		if (!event.getEntityPlayer().world.isRemote) {
			ThirstStats stats = CraftingLifeMod.proxy.getStatsByUUID(event.getEntityPlayer().getUniqueID());
			File saveFile = new File(event.getPlayerDirectory(), event.getPlayerUUID() + ".craftinglifethirstmod");
			try {
				String write = CraftingLifeMod.gsonInstance.toJson(stats);
				saveFile.createNewFile();
				BufferedWriter writer = new BufferedWriter(new FileWriter(saveFile));
				writer.write(write);
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}*/
}
