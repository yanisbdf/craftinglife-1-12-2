package fr.yanis.craftinglifemod.common;

import fr.yanis.craftinglifemod.handlers.gamelogic.ThirstStats;
import net.minecraft.entity.player.EntityPlayer;

import java.util.HashMap;
import java.util.UUID;

public class CommonProxy {

	public HashMap<UUID, ThirstStats> loadedPlayers = new HashMap<>();

	public void init(){}

	public void registerRenders(){}

	public void registerEntitiesRenders(){}


	public ThirstStats getStatsByUUID(UUID uuid) {
		ThirstStats stats = loadedPlayers.get(uuid);
		if (stats == null) {
			System.out.println("[Thirst Mod] Error: Attempted to access non-existent player with UUID: " + uuid);
			return null;
		}
		return stats;
	}

	public void registerPlayer(EntityPlayer player, ThirstStats stats) {
		UUID uuid = player.getUniqueID();
		if (loadedPlayers.containsKey(uuid)) {
			// Player already loaded from previous login session where the
			// server was not closed since the players last login.
			return;
		}
		loadedPlayers.put(uuid, stats);
	}
}
