package fr.yanis.craftinglifemod.client;

import com.leviathanstudio.craftstudio.client.registry.CSRegistryHelper;
import com.leviathanstudio.craftstudio.client.registry.CraftStudioLoader;
import com.leviathanstudio.craftstudio.client.util.EnumRenderType;
import com.leviathanstudio.craftstudio.client.util.EnumResourceType;
import com.leviathanstudio.craftstudio.common.animation.simpleImpl.CSTileEntitySpecialRenderer;
import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.blocks.tileentities.*;
import fr.yanis.craftinglifemod.client.renderers.*;
import fr.yanis.craftinglifemod.client.renderers.tesr.*;
import fr.yanis.craftinglifemod.common.CommonProxy;
import fr.yanis.craftinglifemod.entities.EntityDealer;
import fr.yanis.craftinglifemod.entities.EntityItemAdv;
import fr.yanis.craftinglifemod.handlers.gamelogic.ThirstStats;
import net.minecraft.client.renderer.tileentity.TileEntityItemStackRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy {

	public static ThirstStats clientStats = new ThirstStats();

	@Override
	public void init() {
		TileEntityItemStackRenderer.instance = new TileEntityInventoryRenderHelper();
	}

	@CraftStudioLoader
	public static void registerCraftStudioAssets() {
		CSRegistryHelper registry = new CSRegistryHelper(CraftingLifeMod.MODID);
		registry.register(EnumResourceType.MODEL, EnumRenderType.BLOCK, "flag");
		registry.register(EnumResourceType.MODEL, EnumRenderType.BLOCK, "manualbarrier");
		registry.register(EnumResourceType.MODEL, EnumRenderType.BLOCK, "atm");
		/*registry.register(EnumResourceType.MODEL, EnumRenderType.BLOCK, "loudspeaker");
		registry.register(EnumResourceType.MODEL, EnumRenderType.BLOCK, "speaker_computer");*/

		registry.register(EnumResourceType.ANIM, EnumRenderType.BLOCK, "flag_anim");
		registry.register(EnumResourceType.ANIM, EnumRenderType.BLOCK, "manualbarrier_anim");
		registry.register(EnumResourceType.ANIM, EnumRenderType.BLOCK, "manualbarrier_anim_closed");
	}
	
	@Override
	public void registerRenders(){
		ClientRegistry.bindTileEntitySpecialRenderer(TECocaDispenser.class, new TESRCocaDispenser());
		ClientRegistry.bindTileEntitySpecialRenderer(TECoffeeDispenser.class, new TESRCoffeeDispenser());
		ClientRegistry.bindTileEntitySpecialRenderer(TEDeskD.class, new TESRDeskD());
		ClientRegistry.bindTileEntitySpecialRenderer(TEDisplay.class, new TESRDisplay());
		ClientRegistry.bindTileEntitySpecialRenderer(TEDrillMachine.class, new TESRDrillMachine());
		ClientRegistry.bindTileEntitySpecialRenderer(TEElectricityMeter.class, new TESRElectricityMeter());
		ClientRegistry.bindTileEntitySpecialRenderer(TEFireExtinguisher.class, new TESRFireExtinguisher());
		ClientRegistry.bindTileEntitySpecialRenderer(TEFuelPump.class, new TESRFuelPump());
		ClientRegistry.bindTileEntitySpecialRenderer(TEFoodStand.class, new TESRFoodStand());
		ClientRegistry.bindTileEntitySpecialRenderer(TEFootBallCage.class, new TESRFootBallCage());
		ClientRegistry.bindTileEntitySpecialRenderer(TEGarage.class, new TESRGarage());
		ClientRegistry.bindTileEntitySpecialRenderer(TEGarbageBin.class, new TESRGarbageBin());
		ClientRegistry.bindTileEntitySpecialRenderer(TELockers.class, new TESRLockers());
		ClientRegistry.bindTileEntitySpecialRenderer(TEGrapePress.class, new TESRGrapePress());
		ClientRegistry.bindTileEntitySpecialRenderer(TEAtm.class, new TESRPhoneF());
		ClientRegistry.bindTileEntitySpecialRenderer(TEPlainware.class, new TESRPlainware());
		ClientRegistry.bindTileEntitySpecialRenderer(TESwitch.class, new TESRSwitch());
		ClientRegistry.bindTileEntitySpecialRenderer(TECameraB.class, new TESRCameraB());
		ClientRegistry.bindTileEntitySpecialRenderer(TETrafficLight.class, new TESRTrafficLight());
		ClientRegistry.bindTileEntitySpecialRenderer(TEVentilation.class, new TESRVentilation());

		ClientRegistry.bindTileEntitySpecialRenderer(TEFlag.class, new CSTileEntitySpecialRenderer(CraftingLifeMod.MODID, "flag", 128,
			128, new ResourceLocation(CraftingLifeMod.MODID, "textures/models/blocks/flag.png")));
		ClientRegistry.bindTileEntitySpecialRenderer(TEAtm.class, new TESRAtm(CraftingLifeMod.MODID, "atm", 128,
			128, new ResourceLocation(CraftingLifeMod.MODID, "textures/models/blocks/atm.png")));
		ClientRegistry.bindTileEntitySpecialRenderer(TEFence.class, new CSTileEntitySpecialRenderer(CraftingLifeMod.MODID, "manualbarrier", 128,
			128, new ResourceLocation(CraftingLifeMod.MODID, "textures/models/blocks/manualbarrier.png")));		/*ClientRegistry.bindTileEntitySpecialRenderer(TEFence.class, new CSTileEntitySpecialRendererNoAnimation(ServalMod.MODID, "loudspeaker", 128,
			128, new ResourceLocation(ServalMod.MODID, "textures/models/block/loudspeaker.png")));
		ClientRegistry.bindTileEntitySpecialRenderer(TESpeakersComputer.class, new CSTileEntitySpecialRendererNoAnimation(ServalMod.MODID, "speaker_computer", 128,
			128, new ResourceLocation(ServalMod.MODID, "textures/models/block/speaker_computer.png")));*/

	}



	@Override
	public void registerEntitiesRenders(){
		RenderingRegistry.registerEntityRenderingHandler(EntityDealer.class, new RenderDealer.Factory());
		RenderingRegistry.registerEntityRenderingHandler(EntityItemAdv.class, new RenderItemAdv.Factory());
	}


}
