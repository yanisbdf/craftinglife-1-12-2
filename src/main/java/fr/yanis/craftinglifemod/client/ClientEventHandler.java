package fr.yanis.craftinglifemod.client;

import com.flansmod.common.driveables.EntitySeat;
import com.flansmod.common.driveables.EntityVehicle;
import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.client.renderers.RenderItemAdv;
import fr.yanis.craftinglifemod.entities.EntityItemAdv;
import fr.yanis.craftinglifemod.handlers.gamelogic.ThirstStats;
import fr.yanis.craftinglifemod.network.PacketConcessVehicList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.lwjgl.opengl.GL11;

public class ClientEventHandler {

	private Minecraft mc = Minecraft.getMinecraft();
	private static final ResourceLocation icons = new ResourceLocation(CraftingLifeMod.MODID, "textures/gui/icons.png");

	// HUD
	@SubscribeEvent
	public void onRenderHUD(RenderGameOverlayEvent event) {
		if (event.getType() == RenderGameOverlayEvent.ElementType.HEALTH || event.getType() == RenderGameOverlayEvent.ElementType.FOOD || event.getType() == RenderGameOverlayEvent.ElementType.ARMOR
				|| event.getType() == RenderGameOverlayEvent.ElementType.EXPERIENCE) {
			event.setCanceled(true);

			ScaledResolution sr = new ScaledResolution(Minecraft.getMinecraft());
			int Width = sr.getScaledWidth();
			int Height = sr.getScaledHeight();
			GuiIngame localGuiIngame = Minecraft.getMinecraft().ingameGUI;
			Minecraft.getMinecraft().getTextureManager().bindTexture(icons);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			if (event.getType() == RenderGameOverlayEvent.ElementType.FOOD) {
				localGuiIngame.drawTexturedModalRect(Width - 32, Height / 2 + 80, 30, 0, 13, 20);
				FMLClientHandler.instance().getClient().fontRenderer.drawString("§b" + Math.round(Minecraft.getMinecraft().player.getFoodStats().getFoodLevel() * 5) + "", Width - 20, Height / 2 + 87, 5132111);
			} else if (event.getType() == RenderGameOverlayEvent.ElementType.HEALTH) {
				localGuiIngame.drawTexturedModalRect(Width - 33, Height / 2 + 67, 7, 0, 15, 94);
				FMLClientHandler.instance().getClient().fontRenderer.drawString("§b" + Math.round(Minecraft.getMinecraft().player.getHealth() * 5.0F) + "", Width - 20, Height / 2 + 75, 5132111);
			} else if (event.getType() == RenderGameOverlayEvent.ElementType.ARMOR) {
				localGuiIngame.drawTexturedModalRect(Width - 32, Height / 2 + 93, 50, 0, 20, 256);
				FMLClientHandler.instance().getClient().fontRenderer.drawString("§b" + Minecraft.getMinecraft().player.getTotalArmorValue() * 5 + "", Width - 18, Height / 2 + 102, 16777215);
			} else if (Minecraft.getMinecraft().playerController.isNotCreative()) {
				localGuiIngame.drawTexturedModalRect(Width / 230, Height / 2 + 93, 70, 0, 11, 17);
				//FMLClientHandler.instance().getClient().fontRenderer.drawString("§b" + (ThirstStats.THIRST_DEFAULT_MAX_VALUE - props.thirstSystem.getCurrentThirstLevel()) + "", Width / 230 + 15, Height / 2 + 100, 16777215);
			}

		}

	}

	// VehicleHUD
	@SubscribeEvent
	public void onRenderVehiculeHUD(RenderGameOverlayEvent.Post event) {
		if ((event.getType() == RenderGameOverlayEvent.ElementType.ALL) && mc.player.getRidingEntity() instanceof EntitySeat) {
			EntitySeat seat = (EntitySeat) mc.player.getRidingEntity();
			if (seat.driveable instanceof EntityVehicle) {
				EntityVehicle v = (EntityVehicle) seat.driveable;
				int essence = Math.round(v.driveableData.fuelInTank);
				ScaledResolution sr = new ScaledResolution(Minecraft.getMinecraft());
				int Width = sr.getScaledWidth();
				int Height = sr.getScaledHeight();
				int x = 40 / 6;
				int y = 40 / 6;
				this.mc.renderEngine.bindTexture(new ResourceLocation(CraftingLifeMod.MODID, "textures/gui/vehicleoverlay.png"));
				FMLClientHandler.instance().getClient().ingameGUI.drawTexturedModalRect(0, 0, 0, 0, 140, 174);

				String vehicleName = I18n.format("item." + v.getVehicleType().shortName + ".name", (Object[]) new Object[0]);
				if (!v.isVehicleLocked()) {
					FMLClientHandler.instance().getClient().ingameGUI.drawTexturedModalRect(102, 57, 160, 0, 50, 100);
				} else {
					FMLClientHandler.instance().getClient().ingameGUI.drawTexturedModalRect(108, 57, 145, 0, 10, 100);
				}

				double speed = v.throttle * 122;

				GL11.glPushMatrix();
				GL11.glScalef(2F, 2F, 1);
				if (speed >= 0) {
					FMLClientHandler.instance().getClient().fontRenderer.drawString((int) speed + " km/h", x + 13, y + 10, 16777215);
				} else if (speed < 0) {
					FMLClientHandler.instance().getClient().fontRenderer.drawString((int) Math.abs(speed) + " km/h", x + 13, y + 10, 0xffffff);
					GL11.glScalef(0.4F, 0.4F, 1F);
					FMLClientHandler.instance().getClient().fontRenderer.drawString("R", x + 30, y + 45, 0xffffff);
				}
				GL11.glPopMatrix();

				GL11.glPushMatrix();
				GL11.glScalef(0.5F, 0.5F, 1);
				FMLClientHandler.instance().getClient().fontRenderer.drawString("Réservoir", x + 112, y + 103, 0x3498db);
				FMLClientHandler.instance().getClient().fontRenderer.drawString(getWorldHours(mc.world) + "h:" + getWorldMinutes(mc.world) + "m", x + 35, y + 125, 0x3498db);
				FMLClientHandler.instance().getClient().fontRenderer.drawString(vehicleName, x + 15, y + 17, 16777215);
				GL11.glPopMatrix();

				GL11.glPushMatrix();
				GL11.glScalef(0.7F, 0.7F, 1);
				FMLClientHandler.instance().getClient().fontRenderer.drawString(essence + "L", x + 85, y + 87, 0x3498db);
				GL11.glPopMatrix();

			}

		}
	}

	@SubscribeEvent
	public void onItemPickup(EntityItemPickupEvent event)
	{
		if(event.getEntity() instanceof EntityPlayer)
		{
			event.setCanceled(true);
		}
	}

	@SubscribeEvent
	public void onEntitySpawn(EntityJoinWorldEvent event)
	{
		if(!event.getWorld().isRemote && event.getEntity() instanceof net.minecraft.entity.item.EntityItem)
		{
			event.setCanceled(true); //SPONGE COMPATIBLITY should disable this and use setDead() : CALL THIS CANCELS ALL DROP AND RESTORES PLAYER INV

			Entity entity = new EntityItemAdv(event.getWorld() ,event.getEntity().posX, event.getEntity().posY, event.getEntity().posZ, ((net.minecraft.entity.item.EntityItem)event.getEntity()).getItem());
			entity.motionX = event.getEntity().motionX;
			entity.motionY = event.getEntity().motionY;
			entity.motionZ = event.getEntity().motionZ;
			event.getWorld().spawnEntity(entity);
		}
		/*else if(!event.getWorld().isRemote && event.entity instanceof EntityPlayerMP)
		{
			NBTTagCompound data = new NBTTagCompound();
			WorldSaveData.instance().writeToNBT(data);
			data.setInteger("Action", 2);
			CraftingMod.network.sendTo(new PacketConcessVehicList(data), (EntityPlayerMP) event.entity);
		}*/
	}

	@SubscribeEvent(priority = EventPriority.NORMAL)
	public void onRender(RenderGameOverlayEvent.Post event)
	{
		ScaledResolution sr = new ScaledResolution(Minecraft.getMinecraft());
		int X = sr.getScaledWidth() / 2;
		int Y = sr.getScaledHeight();
		if(event.getType() == RenderGameOverlayEvent.ElementType.ALL)
		{
			//System.out.println(Minecraft.getMinecraft().objectMouseOver);
			if(Minecraft.getMinecraft().objectMouseOver != null && Minecraft.getMinecraft().objectMouseOver.typeOfHit == RayTraceResult.Type.ENTITY && Minecraft.getMinecraft().objectMouseOver.entityHit instanceof EntityItemAdv)
			{
				RenderItemAdv.hoveredEntity = (EntityItemAdv) Minecraft.getMinecraft().objectMouseOver.entityHit;
				String text = "§f[§6Clique droit§f] pour ramasser §6" + RenderItemAdv.hoveredEntity.getItem().getDisplayName() + "[x" + RenderItemAdv.hoveredEntity.getItem().getCount() + "]";
				FMLClientHandler.instance().getClient().fontRenderer.drawString(text, X - FMLClientHandler.instance().getClient().fontRenderer.getStringWidth(text) / 2, Y - 32, 0);
			}
			else
				RenderItemAdv.hoveredEntity = null;
		}
	}

	public static int getWorldMinutes(World world)
	{
		int time = (int)Math.abs((world.getWorldTime() + 6000) % 24000);
		return (time % 1000) * 6 / 100;
	}

	public static int getWorldHours(World world)
	{
		int time = (int)Math.abs((world.getWorldTime() + 6000) % 24000);
		return (int)((float)time / 1000F);
	}


}
