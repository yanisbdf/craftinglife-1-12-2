package fr.yanis.craftinglifemod.client.renderers;

import com.flansmod.client.model.RenderBullet;
import com.flansmod.client.model.RenderVehicle;
import com.flansmod.common.FlansMod;

import com.flansmod.common.driveables.EntityVehicle;
import com.flansmod.common.guns.EntityBullet;
import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.entities.EntityDealer;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.IRenderFactory;

import javax.annotation.Nullable;

public class RenderDealer<T extends EntityLiving> extends RenderLiving<T> {

    public ResourceLocation texture = new ResourceLocation(CraftingLifeMod.MODID, "textures/entity/skindon.png");
    public final ResourceLocation defaultTexture = new ResourceLocation(CraftingLifeMod.MODID, "textures/entity/skindon.png");

	public RenderDealer(RenderManager renderManagerIn, ModelBiped modelBipedIn, float shadowSize) {
		super(renderManagerIn, modelBipedIn, shadowSize);
    }

	@Nullable
	@Override
	protected ResourceLocation getEntityTexture(T entity) {
		switch(((EntityDealer)entity).getSkin()){
			case 1:
				texture = new ResourceLocation(CraftingLifeMod.MODID, "textures/entity/police.png");
				return texture;

			case 2:
				texture = new ResourceLocation(CraftingLifeMod.MODID, "textures/entity/pompier.png");
				return texture;

           /* case 3:
                texture = new ResourceLocation(FlansMod.MODID, "textures/entity/" + GuiNPC.texture.getText() + ".png");
                return texture;

            case 4:
                texture = new ResourceLocation(FlansMod.MODID, "textures/entity/" + GuiNPC.texture.getText() + ".png");
                return texture;

            case 5:
                texture = new ResourceLocation(FlansMod.MODID, "textures/entity/" + GuiNPC.texture.getText() + ".png");
                return texture;*/
		}
		return defaultTexture;
	}

	public static class Factory implements IRenderFactory<EntityDealer>
	{
		@Override
		public Render<EntityDealer> createRenderFor(RenderManager manager)
		{
			return new RenderDealer(manager, new ModelBiped(), 1.0F);
		}
	}

}
