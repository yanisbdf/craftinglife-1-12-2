package fr.yanis.craftinglifemod.client.renderers.tesr;

import com.leviathanstudio.craftstudio.client.model.ModelCraftStudio;
import com.leviathanstudio.craftstudio.client.util.MathHelper;
import com.leviathanstudio.craftstudio.common.animation.simpleImpl.CSTileEntitySpecialRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import javax.vecmath.Matrix4f;
import java.nio.FloatBuffer;

public class TESRAtm<T extends TileEntity> extends TileEntitySpecialRenderer<T>
{
	/** Efficient rotation corrector, you can use it in your renderer. */
	public static final FloatBuffer ROTATION_CORRECTOR;
	static {
		Matrix4f mat = new Matrix4f();
		mat.set(MathHelper.quatFromEuler(180, 0, 0));
		ROTATION_CORRECTOR = MathHelper.makeFloatBuffer(mat);
	}

	/** The model of the block. */
	protected ModelCraftStudio model;
	/** The texture of the block */
	protected ResourceLocation texture;

	/** The constructor that initialize the model and save texture. */
	public TESRAtm(String modid, String modelNameIn, int textureWidth, int textureHeigth, ResourceLocation texture) {
		this.model = new ModelCraftStudio(modid, modelNameIn, textureWidth, textureHeigth);
		this.texture = texture;
	}

	@Override
	public void render(T te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
		GlStateManager.pushMatrix();
		// Correction of the position.
		GlStateManager.translate(x + 0.5D, y + 1.5D, z + 0.5D);
		// Correction of the rotation.
		GlStateManager.multMatrix(CSTileEntitySpecialRenderer.ROTATION_CORRECTOR);
		this.bindTexture(this.texture); // Binding the texture.
		this.model.render(); // Rendering the model.
		GlStateManager.popMatrix();
	}
}
