package fr.yanis.craftinglifemod.client.renderers;

import com.flansmod.client.model.RenderGunItem;
import fr.yanis.craftinglifemod.entities.EntityItemAdv;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import javax.annotation.Nullable;
import java.awt.*;
import java.util.Random;

public class RenderItemAdv extends Render
{

	public static EntityItemAdv hoveredEntity;
	private final Random random = new Random();
	private final RenderItem itemRenderer;

	public RenderItemAdv(RenderManager renderManagerIn, RenderItem renderItem)
	{
		super(renderManagerIn);
		itemRenderer = renderItem;
	}

	public void doRender(Entity entity, double x, double y, double z, float entityYaw, float partialTicks) {
		ItemStack s = ((EntityItemAdv)entity).getItem();
		if(s != null)
		{

			double adjustY = ((double)((EntityItemAdv)entity).getRandomRotation())/1800;

			GL11.glPushMatrix();
			GL11.glTranslated(x, (double) (y+0.035D-adjustY), z);
			GL11.glRotated(2*((EntityItemAdv)entity).getRandomRotation(), 0, 1, 0);
			GL11.glRotated(90, 1, 0, 0);
			GL11.glScaled(0.60, 0.60, 0.60);
			renderItem((EntityItemAdv)entity, x, y, z, partialTicks);
			GL11.glPopMatrix();

			renderNameAdv((EntityItemAdv) entity, x, y, z);

		}
	}


	protected void renderNameAdv(EntityItemAdv entity, double x, double y, double z) {
		if(entity == hoveredEntity)
			renderName(entity, x, y, z);
	}
	private void renderItem(EntityItemAdv entity, double x, double y, double z, float partialTicks){
		ItemStack itemstack = entity.getItem();
		int i = itemstack.isEmpty() ? 187 : Item.getIdFromItem(itemstack.getItem()) + itemstack.getMetadata();
		this.random.setSeed((long)i);
		boolean flag = false;

		if (this.bindEntityTexture(entity))
		{
			this.renderManager.renderEngine.getTexture(this.getEntityTexture(entity)).setBlurMipmap(false, false);
			flag = true;
		}

		GlStateManager.enableRescaleNormal();
		GlStateManager.alphaFunc(516, 0.1F);
		GlStateManager.enableBlend();
		RenderHelper.enableStandardItemLighting();
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
		GlStateManager.pushMatrix();
		IBakedModel ibakedmodel = this.itemRenderer.getItemModelWithOverrides(itemstack, entity.world, (EntityLivingBase)null);
		int j = this.transformModelCount(entity, partialTicks, ibakedmodel);
		boolean flag1 = ibakedmodel.isGui3d();
		double f8 = 1.5D;

		if (this.renderOutlines)
		{
			GlStateManager.enableColorMaterial();
			GlStateManager.enableOutlineMode(this.getTeamColor(entity));
		}

		for (int k = 0; k < j; ++k)
		{
			if (flag1)
			{
				GlStateManager.pushMatrix();
				GlStateManager.scale(f8, f8, f8);
				GlStateManager.translate(0.0F, -0.2F, -0.1F);
				IBakedModel transformedModel = net.minecraftforge.client.ForgeHooksClient.handleCameraTransforms(ibakedmodel, ItemCameraTransforms.TransformType.GROUND, false);
				this.itemRenderer.renderItem(itemstack, transformedModel);
				GlStateManager.popMatrix();
			}
			else
			{
				GlStateManager.pushMatrix();
				GlStateManager.scale(f8, f8, f8);
				GlStateManager.translate(0.0F, -0.2F, -0.1F);
				IBakedModel transformedModel = net.minecraftforge.client.ForgeHooksClient.handleCameraTransforms(ibakedmodel, ItemCameraTransforms.TransformType.GROUND, false);
				this.itemRenderer.renderItem(itemstack, transformedModel);
				GlStateManager.popMatrix();
				GlStateManager.translate(0.0F, 0.0F, 0.09375F);
			}
		}

		if (this.renderOutlines)
		{
			GlStateManager.disableOutlineMode();
			GlStateManager.disableColorMaterial();
		}

		GlStateManager.popMatrix();
		GlStateManager.disableRescaleNormal();
		GlStateManager.disableBlend();
		this.bindEntityTexture(entity);

		if (flag)
		{
			this.renderManager.renderEngine.getTexture(this.getEntityTexture(entity)).restoreLastBlurMipmap();
		}

	}

	private int transformModelCount(EntityItemAdv itemIn, float partialTicks, IBakedModel ibakedmodel)
	{
		ItemStack itemstack = itemIn.getItem();
		Item item = itemstack.getItem();

		if (item == null)
		{
			return 0;
		}
		else
		{
			boolean flag = ibakedmodel.isGui3d();
			int i = this.getModelCount(itemstack);
			float f = 0.25F;
			float f1 = shouldBob() ? MathHelper.sin(((float)itemIn.getAge() + partialTicks) / 10.0F + itemIn.hoverStart) * 0.1F + 0.1F : 0;
			float f2 = ibakedmodel.getItemCameraTransforms().getTransform(ItemCameraTransforms.TransformType.GROUND).scale.y;
			//GlStateManager.translate((float)p_177077_2_, (float)p_177077_4_ + f1 + 0.25F * f2, (float)p_177077_6_);

			if (flag || this.renderManager.options != null)
			{
				float f3 = (((float)itemIn.getAge() + partialTicks) / 20.0F + itemIn.hoverStart) * (180F / (float)Math.PI);
				//GlStateManager.rotate(f3, 0.0F, 1.0F, 0.0F);
			}

			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			return i;
		}
	}

	protected int getModelCount(ItemStack stack)
	{
		int i = 1;

		if (stack.getCount() > 48)
		{
			i = 5;
		}
		else if (stack.getCount() > 32)
		{
			i = 4;
		}
		else if (stack.getCount() > 16)
		{
			i = 3;
		}
		else if (stack.getCount() > 1)
		{
			i = 2;
		}

		return i;
	}

	/**
	 * Items should spread out when rendered in 3d?
	 * @return
	 */
	public boolean shouldSpreadItems()
	{
		return true;
	}

	/**
	 * Items should have a bob effect
	 * @return
	 */
	public boolean shouldBob()
	{
		return true;
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return TextureMap.LOCATION_BLOCKS_TEXTURE;
	}

	public static class Factory implements IRenderFactory<EntityItemAdv>
	{
		@Override
		public Render<EntityItemAdv> createRenderFor(RenderManager manager)
		{
			return new RenderItemAdv(manager, Minecraft.getMinecraft().getRenderItem());
		}
	}
}
