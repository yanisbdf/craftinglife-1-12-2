package fr.yanis.craftinglifemod.client.renderers.tesr;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.blocks.tileentities.TECocaDispenser;
import fr.yanis.craftinglifemod.blocks.tileentities.TECoffeeDispenser;
import fr.yanis.craftinglifemod.client.model.ModelCafe;
import fr.yanis.craftinglifemod.client.model.ModelDistributeur;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class TESRCoffeeDispenser  extends TileEntitySpecialRenderer {

	public static ModelCafe model = new ModelCafe();
	public static ResourceLocation texture = new ResourceLocation(CraftingLifeMod.MODID, "textures/models/blocks/model_cafe.png");

	@Override
	public void render(TileEntity tile, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		this.renderTileEntityAt((TECoffeeDispenser)tile, x, y, z, partialTicks, destroyStage, alpha);
	}

	private void renderTileEntityAt(TECoffeeDispenser tile, double x, double y, double z, float partialTick, int destroyStage, float alpha)
	{
		GL11.glPushMatrix();
		GL11.glTranslated(x + 0.5D, y + 1.5D, z + 0.5D);
		GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
		GL11.glRotatef(10F * tile.getDirection(), 0.0F, 1.0F, 0.0F);
		this.bindTexture(texture);
		model.renderAll();
		GL11.glPopMatrix();

	}
}
