package fr.yanis.craftinglifemod.client.renderers;

import fr.yanis.craftinglifemod.blocks.CraftingLifeBlocks;
import fr.yanis.craftinglifemod.blocks.tileentities.*;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntityItemStackRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class TileEntityInventoryRenderHelper extends TileEntityItemStackRenderer {

	@Override
	public void renderByItem(ItemStack itemStack) {
		Block block = Block.getBlockFromItem(itemStack.getItem());

		//System.out.println(itemStack.getItem().getTranslationKey());
		/*if(block == CraftingLifeBlocks.TRAFFICLIGHT)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.5, 0.5, 0.5);
			GL11.glRotatef(0F, 0.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -2.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TETrafficLight(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}*/

		if(block == CraftingLifeBlocks.LOCKERS)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.5, 0.5, 0.5);
			GL11.glRotatef(180F, 90.0F, 0.0F, 1.0F);
			GL11.glTranslatef(-0.5F, -0.25F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TELockers(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.GRAPEPRESS)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.5, 0.5, 0.5);
			GL11.glRotatef(180F, 90.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.5F, -1.2F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEGrapePress(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		/*if(block == CraftingLifeBlocks.blockFeuG2)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.5, 0.5, 0.5);
			GL11.glRotatef(0F, 0.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -2.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TETrafficLight(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}*/

		if(block == CraftingLifeBlocks.FLAG)
		{
			GL11.glPushMatrix();
			GlStateManager.scale(0.12,0.12,0.12);
			GL11.glRotatef( 40F, 0.0F, 1.0F, 0.0F);
			GL11.glTranslatef(4F, 0.3F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEFlag(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.DRILLMACHINE)
		{
			GL11.glPushMatrix();
			GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -1.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEDrillMachine(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.VENTILATION)
		{
			GL11.glPushMatrix();
			GL11.glRotatef(180F, 90.0F, 0.0F, 1.0F);
			GL11.glTranslatef(-0.9F, -1.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEVentilation(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.CAMERA)
		{
			GL11.glPushMatrix();
			GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -1.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TECameraB(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.GARAGE)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.8, 0.8, 0.8);
			GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -1.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEGarage(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.FOODSTAND)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.8, 0.8, 0.8);
			GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -1.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEFoodStand(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.FOOTBALLCAGE)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.4, 0.4, 0.4);
			GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
			GL11.glTranslatef(-1.5F, -1.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEFoodStand(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.ELECTRICITYMETER)
		{
			GL11.glPushMatrix();
			GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.5F, -1.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEElectricityMeter(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.FENCE)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.6, 0.6, 0.6);
			//GL11.glRotatef(0F, 90.0F, 90.0F, 1.0F);
			//GL11.glRotatef(180F, 0F, 0.1F, 1F);
			//GL11.glRotatef(0F, 1F, 0F, 0F);
			GL11.glRotatef(100, 0F, 1F, 0F);
			GL11.glTranslatef(-5F, 0.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEFence(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.COFFEEDISPENSER)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.4, 0.4, 0.4);
			GL11.glRotatef(180F, 90.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, 1.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TECoffeeDispenser(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.FUELPUMP)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.6, 0.6, 0.6);
			GL11.glRotatef(180F, 90.0F, 0.0F, 1.0F);
			GL11.glTranslatef(-0.2F, -1.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEFuelPump(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}
		if(block == CraftingLifeBlocks.COCADISPENSER)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.7, 0.7, 0.7);
			GL11.glRotatef(180F, 90.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -0.5F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TECocaDispenser(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}
		if(block == CraftingLifeBlocks.DISPLAY)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.4, 0.4, 0.4);
			GL11.glRotatef(180F, 90.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -1.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEDisplay(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}

		if(block == CraftingLifeBlocks.GARBAGEBIN)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.42, 0.42, 0.42);
			GL11.glRotatef(0.0F, 0.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -1.5F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEGarbageBin(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}


		/*if(block == CraftingLifeBlocks.FIREEXTINGUISHER)
		{
			GL11.glPushMatrix();
			GL11.glScaled(0.4, 0.4, 0.4);
			GL11.glRotatef(0.0F, 0.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -1.5F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEFireExtinguisher(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}*/

		if(block == CraftingLifeBlocks.GASOLINEJERRYCAN)
		{
			GL11.glPushMatrix();
			GL11.glScaled(1.5, 1.5, 1.5);
			GL11.glRotatef(0.0F, 0.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -0.5F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEGasolineJerrycan(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}
		if(block == CraftingLifeBlocks.PLAINWARE)
		{
			GL11.glPushMatrix();
			GL11.glScalef(0.5F, 0.5F, 0.5F);
			GL11.glRotatef(180F, 90.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -0.0F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEPlainware(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}
		if(block == CraftingLifeBlocks.ATM)
		{
			GL11.glPushMatrix();
			GL11.glScalef(3F, 3F, 3F);
			GL11.glRotatef(180F, 90.0F, 0.0F, 1.0F);
			GL11.glTranslatef(0.0F, -1.3F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEAtm(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}
		if(block == CraftingLifeBlocks.DESKD)
		{
			GL11.glPushMatrix();
			GL11.glScalef(0.5F, 0.5F, 0.5F);
			GL11.glRotatef(180F, 90.0F, 0.0F, 1.0F);
			GL11.glTranslatef(-0.5F, -0.3F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TEDeskD(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}
		/*if(block == CraftingLifeBlocks.SWITCH)
		{
			GL11.glPushMatrix();
			GL11.glScalef(3F, 3F, 3F);
			GL11.glRotatef(180F, 90.0F, 0.0F, 1.0F);
			GL11.glTranslatef(-0.5F, -0.6F, 0.0F);
			TileEntityRendererDispatcher.instance.render(new TESwitch(), 0.0D, 0.0D, 0.0D, 0.0F);
			GL11.glPopMatrix();
		}*/ else {
			super.renderByItem(itemStack);
		}
	}
}
