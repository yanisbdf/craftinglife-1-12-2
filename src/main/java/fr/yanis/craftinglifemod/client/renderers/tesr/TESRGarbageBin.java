package fr.yanis.craftinglifemod.client.renderers.tesr;

import GLLoader.GLModel;
import fr.yanis.craftinglifemod.blocks.tileentities.TEGarbageBin;
import fr.yanis.craftinglifemod.blocks.tileentities.TEGrapePress;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import org.lwjgl.opengl.GL11;

public class TESRGarbageBin extends TileEntitySpecialRenderer {

	public static GLModel model;

	public TESRGarbageBin()
	{
		model = new GLModel("/assets/obj/trashcan.obj");
	}

	@Override
	public void render(TileEntity tile, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		this.renderTileEntityTrashAt((TEGarbageBin)tile, x, y, z, partialTicks, destroyStage, alpha);
	}

	public void renderTileEntityTrashAt(TEGarbageBin tileentity, double x, double y, double z, float partialTick, int destroyStage, float alpha)
	{
		GL11.glPushMatrix();
		GL11.glTranslated(x+0.5, y, z+0.5);
		GL11.glScaled(0.33, 0.33, 0.33);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		model.render();
		GL11.glPopMatrix();
	}
}
