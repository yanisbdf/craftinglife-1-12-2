package fr.yanis.craftinglifemod.client.renderers.tesr;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.blocks.tileentities.TEDeskD;
import fr.yanis.craftinglifemod.blocks.tileentities.TEDisplay;
import fr.yanis.craftinglifemod.client.model.ModelBureauD;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class TESRDeskD extends TileEntitySpecialRenderer {

	public static ModelBureauD model = new ModelBureauD();
	public static ResourceLocation texture = new ResourceLocation(CraftingLifeMod.MODID, "textures/models/blocks/model_bureaud.png");

	@Override
	public void render(TileEntity tile, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		this.renderTileEntityAt((TEDeskD)tile, x, y, z, partialTicks, destroyStage, alpha);
	}

	private void renderTileEntityAt(TEDeskD tile, double x, double y, double z, float partialTick, int destroyStage, float alpha)
	{
		GL11.glPushMatrix();
		GL11.glTranslated(x + 0.5D, y + 1.5D, z + 0.5D);
		GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
		GL11.glRotatef(10F * tile.getDirection(), 0.0F, 1.0F, 0.0F);
		this.bindTexture(texture);
		model.renderAll();
		GL11.glPopMatrix();

	}
}
