package fr.yanis.craftinglifemod.client.renderers.tesr;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.blocks.tileentities.TECameraB;
import fr.yanis.craftinglifemod.blocks.tileentities.TEGrapePress;
import fr.yanis.craftinglifemod.client.model.ModelCamera;
import fr.yanis.craftinglifemod.client.model.ModelGrapePress;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class TESRCameraB extends TileEntitySpecialRenderer {

	public static ModelCamera model = new ModelCamera();
	public static ResourceLocation texture = new ResourceLocation(CraftingLifeMod.MODID, "textures/models/blocks/model_camera.png");

	@Override
	public void render(TileEntity tile, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		this.renderTileEntityCasierAt((TECameraB)tile, x, y, z, partialTicks, destroyStage, alpha);
	}

	private void renderTileEntityCasierAt(TECameraB tile, double x, double y, double z, float partialTick, int destroyStage, float alpha)
	{

		GL11.glPushMatrix();
		GL11.glTranslated(x + 0.5D, y + 1.5D, z + 0.5D);
		GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
		GL11.glRotatef(180F + 90F * tile.getDirection(), 0.0F, 1.0F, 0.0F);
		GL11.glTranslated(0, 0, -0.5);
		this.bindTexture(texture);
		model.renderAll();
		GL11.glPopMatrix();



	}
}
