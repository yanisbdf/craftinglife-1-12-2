package fr.yanis.craftinglifemod.client.gui;

import java.io.IOException;
import java.util.List;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.entities.EntityDealer;
import fr.yanis.craftinglifemod.network.PacketConcessVehicList;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.client.config.GuiCheckBox;
import org.apache.commons.lang3.StringUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import com.flansmod.common.FlansMod;
import com.flansmod.common.driveables.ItemVehicle;
import com.flansmod.common.driveables.VehicleType;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.GuiScreenEvent.ActionPerformedEvent;
import net.minecraftforge.common.MinecraftForge;

public class GuiAdminDealer extends GuiScreen {
    private EntityDealer concessentity;
    private EntityPlayer playerEntity;
    private GuiTextField price;
    private GuiCheckBox present;
    private Minecraft mc;
    private GuiButton buttonConfirm;
    private GuiCustomSelectedButton bouttonVehicle;
    private String vehiculename;
    private int lastClicked;
    private ResourceLocation texture = new ResourceLocation(CraftingLifeMod.MODID, "textures/gui/concessionnaire.png");
    private boolean found = false;

    public GuiAdminDealer(EntityPlayer ep, EntityDealer concessentity)
    {
        this.playerEntity = ep;
        this.mc = Minecraft.getMinecraft();
        this.concessentity = concessentity;
    }

    @Override
    public boolean doesGuiPauseGame()
    {
        return false;
    }

    @Override
    public void initGui()
    {
        super.initGui();

        int X = (width - 256) / 2;
        int Y = (height - 256) / 2;

		Mouse.setGrabbed(false);

		this.price = new TextFieldOnlyNumbers(1, this.fontRenderer, width/2 - 5, Y + 218, X+189-width/2, 15);
        price.setMaxStringLength(10);
        price.setText("Prix");
        
        this.buttonList.add(present = new GuiCheckBox(1, X+100, Y+30, "Activé dans le concessionaire", false));
        /*this.buttonList.add(gendarmerie = new GuiCheckBox(1, X+100, Y+45, "Gendarmerie", false));
        this.buttonList.add(pompier = new GuiCheckBox(1, X+100, Y+60, "Pompier", false));
        this.buttonList.add(luxe = new GuiCheckBox(1, X+100, Y+75, "Luxe", false));
        this.buttonList.add(camion = new GuiCheckBox(1, X+100, Y+90, "Camion", false));*/
        
        this.buttonList.add(buttonConfirm = new GuiTypeButton(50, X + 189, Y + 214, 60, 20, "Enregistrer", 1));
        this.buttonList.add(new GuiTypeButton(51, X + 226, Y+8, 25, 7, "", 2));
        
        this.present.visible = false;
        this.price.setVisible(false); this.buttonConfirm.visible = false;

        int i = 0, totalHeight=0;
        int buttonsHeight = 0;
        for(final VehicleType v : VehicleType.types) // Pour chaque v�hicule
        {
            String vehic = v.name;
            if(vehic == null) {
                System.out.println("Error with vehic " + v.shortName + " : name null ! Type: " + v);
                vehic = v.shortName;
                if(v.shortName == null)
                    continue;
            }
            final List<String> lines = GuiConcessionnaire.trimTextToWidth(vehic, 69);
            int height = mc.fontRenderer.FONT_HEIGHT*lines.size()+9;
            this.buttonList.add(new GuiCustomSelectedButton(i + 100, this.width / 2 - 126, this.height/2-110+totalHeight, 100, height, vehic) {
                @Override
                public void drawButton(Minecraft mc, int mx, int my, float partialTicks)
                {  
                    int y = this.y+2;
                    GL11.glPushMatrix();
                    //ItemStack vehicle = new ItemStack(v.getItem());
                    mc.getTextureManager().bindTexture(new ResourceLocation(FlansMod.MODID+":textures/items/"+v.iconPath+".png"));
                    //drawTexturedModelRectFromIcon(xPosition, y, v.item.getIconFromDamage(0), 16, 16);
                    GL11.glScaled(0.05, 0.05, 0.05);
                    GL11.glColor4f(1, 1, 1,1);
                    drawTexturedModalRect(x*20, this.y*20, 0, 0, 256, 256);
                    GL11.glPopMatrix();
                   // y+=mc.fontRenderer.FONT_HEIGHT;
                    for(String line : lines) {
                        drawString(fontRenderer, (this.select?"§3":"§f")+line, x+15, y, 1);
                        y+=mc.fontRenderer.FONT_HEIGHT;
                    }
                }
            });
            totalHeight+=height;
            i++;
        }
        scrollY = 0; //On reset le scroll
        maxScroll = totalHeight-220; //Et on met le scroll max à partir hauteur de tous les boutons
        buttonConfirm.enabled = false; // comme aucun v�hicule est s�lectionn�
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException
    {
        super.keyTyped(typedChar, keyCode);
        this.price.textboxKeyTyped(typedChar, keyCode);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException
    {
        if(button.id == 50)
        {
            try
            {
                if(present.isChecked()) {
                    int pricee = Integer.valueOf(price.getText());
                    concessentity.vehicules.put(vehiculename, pricee);
                    NBTTagCompound data = new NBTTagCompound();
                    data.setInteger("Action", 1);
                    data.setInteger("EntId", concessentity.getEntityId());
                    data.setString("nam", vehiculename);
                    data.setInteger("price", pricee);
                    CraftingLifeMod.getPacketHandler().sendToServer(new PacketConcessVehicList(data));
                    
                    playerEntity.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + TextFormatting.GREEN + "Vous avez bien ajouté le " + vehiculename + " au §aconcessionnaire !"));
                }
                else {
                    concessentity.vehicules.remove(vehiculename);
                    NBTTagCompound data = new NBTTagCompound();
                    data.setInteger("Action", 2);
                    data.setInteger("EntId", concessentity.getEntityId());
                    data.setString("namr", vehiculename);
					CraftingLifeMod.getPacketHandler().sendToServer(new PacketConcessVehicList(data));

                    playerEntity.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + TextFormatting.GREEN + "Vous avez bien retiré le " + vehiculename + " du §aconcessionnaire !"));
                }
            }
            catch(NumberFormatException ex)
            {
                playerEntity.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + TextFormatting.RED + "Le prix est invalide"));
            }
        }
        else if(button.id >= 100) // Les boutons de v�hicules
        {
            bouttonVehicle = (GuiCustomSelectedButton)button;
            boolean oneSelected = bouttonVehicle.select;
            // System.out.println(oneSelected + " " + bouttonVehicle.displayString + " " + bouttonVehicle.id + " "+ bouttonVehicle.select);
            // bouttonVehicle.select = oneSelected;
            vehiculename = oneSelected ? button.displayString : "";
            lastClicked = oneSelected ? button.id : -1;
            for(int k = 0; k < buttonList.size(); k++)
            {
                GuiButton b = (GuiButton)buttonList.get(k);
                if(b.id != lastClicked && b.id >= 100)
                {
                    ((GuiCustomSelectedButton)b).select = false;// !oneSelected;
                }
            }
            buttonConfirm.enabled = oneSelected;// oneSelected;
            
            //System.out.println(WorldSaveData.vehicivil);
            
            this.present.visible = oneSelected;
            this.price.setVisible(oneSelected); this.buttonConfirm.visible = oneSelected;
            if(oneSelected) {
                String price = "0";
                this.present.setIsChecked(concessentity.vehicules.containsKey(button.displayString));
                if(concessentity.vehicules.containsKey(button.displayString))
                    price = ""+concessentity.vehicules.get(button.displayString);
                this.price.setText(price);
            }
        }
        else
        {
            switch(button.id)
            {
                case 51:
                    this.mc.player.closeScreen();
                    break;

                default:
                    break;
            }
        }
        super.actionPerformed(button);
    }

    @Override
    public void updateScreen()
    {
        super.updateScreen();
    }

    private int scrollY = 0;
    private int maxScroll;

    public void drawScreen(int x, int y, float f)
    {
        drawDefaultBackground();

        mc.getTextureManager().bindTexture(texture);

        int X = (width - 256) / 2;
        int Y = (height - 256) / 2;
        this.drawTexturedModalRect(X, Y, 0, 0, 256, 240);
       // System.out.println("Test");
        drawString(mc.fontRenderer, "Gestion du Concessionnaire", X+2, Y+5, 0xFFFFFF);
        drawString(mc.fontRenderer, "Informations", X+92, Y+147, 0xFFFFFF);
        drawRect(this.width / 2 - 43, this.height / 2 - 110, this.width / 2 - 39, this.height / 2 + 110, 0x70000000);
        if(VehicleType.types.size() != 0)
        {
            // System.out.println(vehicleList.size());
            int relativeHeight = (maxScroll%220);//10;
            if(maxScroll <= 0) //Hauteur du carré de la scrollbar en fonction de la hauteur totale des boutons (esthétique)
                relativeHeight = 220;
            //else if(maxScroll < 220)
                //relativeHeight = (-maxScroll%220);
            int relativeY = (this.height / 2 - 110)+(scrollY*(220-relativeHeight)/maxScroll); //Position du carré dans la barre de 220 pixels de haut en fonction du scroll
            drawRect(this.width / 2 - 43, relativeY, this.width / 2 - 39, relativeY+relativeHeight, 0xB0000000);
            //System.out.println(posY);
        }

        GL11.glPushMatrix();
        for (int k = 0; k < this.buttonList.size(); ++k) //Dessin des boutons
        {
            GuiButton bu = (GuiButton) this.buttonList.get(k);
            if(bu.id == 100) { //Quand on atteint les boutons de véhicules
                int e = new ScaledResolution(mc).getScaleFactor(); //Résolution utilisée pour le scissor
                int ye = mc.displayHeight - ((this.height/2-110) + 220) * e;//e*(this.height/2-110);
                GL11.glScissor(e*(this.width / 2 - 126), ye, e*100, e*220);
                GL11.glEnable(GL11.GL_SCISSOR_TEST); //On setup le scissor pour ignorer tout ce qui est en dehors de la zone des boutons de véhicules
                GL11.glTranslated(0, -scrollY, 0);  //Et on translate à l'inverse du scroll pour faire apparaître boutons du bas
            }
            bu.drawButton(this.mc, x, y, f);
        }
        GL11.glDisable(GL11.GL_SCISSOR_TEST);
        GL11.glPopMatrix();
        
        for(VehicleType itemV : VehicleType.types)
        {
            //String nomveh = I18n.format("item." + itemV.type.shortName + ".name", (Object[])new Object[0]);
           
            //System.out.println(itemV.getInfoType().iconPath);
            if(itemV.name == null)
                continue;
            if(itemV.name.equals(vehiculename))
            {
                //if(vehiculename != null)
                {
                    drawString(mc.fontRenderer, "Modèle: §a" + StringUtils.abbreviate(itemV.name,24), this.width / 2 - 30, this.height / 2 + 30, 0xFFFFFF);
                    drawString(mc.fontRenderer, "Places passagers: §a" + itemV.seats.length, this.width / 2 - 30, this.height / 2 +45, 0xFFFFFF);
                    drawString(mc.fontRenderer, "Places dans le coffre: §a" + itemV.numCargoSlots, this.width / 2 - 30, this.height / 2 +60, 0xFFFFFF);
                    drawString(mc.fontRenderer, "Vitesse: §a" + (int)itemV.maxThrottle*122+" km/h", this.width / 2 - 30, this.height / 2 +75, 0xFFFFFF);
                    drawString(mc.fontRenderer, "Prix: ", this.width / 2 - 30, this.height / 2 +95, 0xFFFFFF);
                    GL11.glColor4f(1, 1, 1, 1);
                }
            }
        }
        this.price.drawTextBox();
    }

    @Override
    public void handleMouseInput() throws IOException
    {
        super.handleMouseInput();
        if(maxScroll > 0) { //Si on a besoin de scroll (hauteur boutons > hauteur espace sur le gui)
            int i = Mouse.getEventDWheel()/10;
            if(i != 0)
            {
                scrollY -= i; //On modifie valeur du scroll
                scrollY = Math.max(Math.min(scrollY, maxScroll), 0); //Mais la garde entre 0 et maxScroll
            }
        }
    }
    @Override
    protected void mouseClicked(int mx, int my, int button)
    {
        this.price.mouseClicked(mx, my, button);
        if (button == 0)
        {
            for (int l = 0; l < this.buttonList.size(); ++l)
            {
                GuiButton guibutton = (GuiButton)this.buttonList.get(l);

                if (guibutton.mousePressed(this.mc, mx, my+(guibutton.id>=100?scrollY:0))) //Si le bouton est un bouton de véhicule (id>=100), on prend en compte le scroll
                {
                    ActionPerformedEvent.Pre event = new ActionPerformedEvent.Pre(this, guibutton, this.buttonList);
                    if (MinecraftForge.EVENT_BUS.post(event))
                        break;
                    //NOT VISIBLE this.selectedButton = event.button;
                    event.getButton().playPressSound(this.mc.getSoundHandler());
					try {
						this.actionPerformed(event.getButton());
					} catch (IOException e) {
						e.printStackTrace();
					}
					if (this.equals(this.mc.currentScreen))
                        MinecraftForge.EVENT_BUS.post(new ActionPerformedEvent.Post(this, event.getButton(), this.buttonList));
                }
            }
        }
    }
}
