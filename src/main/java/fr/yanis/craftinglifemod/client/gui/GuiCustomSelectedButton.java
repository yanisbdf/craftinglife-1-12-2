package fr.yanis.craftinglifemod.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;

public class GuiCustomSelectedButton extends GuiButton
{
    public boolean select;

    public GuiCustomSelectedButton(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText)
    {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
    }

    public boolean mousePressed(Minecraft mc, int posX, int posY)
    {
        if(this.enabled && this.visible && posX >= this.x && posY >= this.y && posX < this.x + this.width && posY < this.y + this.height)
        {
            this.select = !this.select;
            return true;
        }
        return false;
    }


	public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks)
    {
        FontRenderer fontrenderer = mc.fontRenderer;

        if (this.select)
            drawCenteredString(fontrenderer, "§3"+this.displayString, this.x + this.width / 2, this.y + (this.height - 8) / 2, 1);
        else
            drawCenteredString(fontrenderer, "§f"+this.displayString, this.x + this.width / 2, this.y + (this.height - 8) / 2, 1);



    }
}
