package fr.yanis.craftinglifemod.client.gui;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiTextField;

public class TextFieldOnlyNumbers extends GuiTextField {

	public TextFieldOnlyNumbers(int componentId, FontRenderer fontrendererObj, int x, int y, int width, int height) {
		super(componentId, fontrendererObj, x, y, width, height);
	}

	public void writeText(String textToWrite)
	    {
		if(!textToWrite.matches("([0-9]+)"))
				return;
				else
				super.writeText(textToWrite);
	    }


}
