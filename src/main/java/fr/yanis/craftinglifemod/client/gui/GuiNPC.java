package fr.yanis.craftinglifemod.client.gui;

import com.flansmod.common.FlansMod;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.entities.EntityDealer;
import fr.yanis.craftinglifemod.network.PacketDealer;
import fr.yanis.craftinglifemod.network.PacketKillEntity;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.input.Mouse;

import java.io.IOException;


public class GuiNPC extends GuiScreen
{

    private EntityPlayer playerEntity;
    private EntityDealer concessentity;
    private Minecraft mc;
    private GuiTextField concess, texture;
    private ResourceLocation textures = new ResourceLocation(CraftingLifeMod.MODID, "textures/gui/oldconcess.png");

    public GuiNPC(EntityPlayer ep, EntityDealer entityConcessionnaire)
    {
        this.playerEntity = ep;
        this.mc = Minecraft.getMinecraft();
        this.concessentity = entityConcessionnaire;

    }

    @Override
    public boolean doesGuiPauseGame()
    {
        return false;
    }

    @Override
    public void initGui()
    {
        int X = (width - 256) / 2;
        int Y = (height - 256) / 2;

		Mouse.setGrabbed(false);

        this.concess = new GuiTextField(1, this.fontRenderer, this.width / 2 - -5, this.height / 2 - 46, 100, 20);
        concess.setMaxStringLength(300);
        if(concessentity.concessName.isEmpty())
            concess.setText("Nom concess");
        else
            concess.setText(concessentity.concessName);
        
        this.texture = new GuiTextField(2, this.fontRenderer, this.width / 2 - -5, this.height / 2 - 20, 100, 20);
        texture.setMaxStringLength(300);
        switch (concessentity.getSkin()) {
        case 1:
            texture.setText("police");
            break;
        case 2:
            texture.setText("pompier");
            break;
        case 3:
            texture.setText("default");
            break;
        default:
            texture.setText("Nom texture");
            break;
        }
        

        this.buttonList.add(new GuiButton(103, X + 3, Y + 70, 102, 20, "Modifier véhicules"));
        this.buttonList.add(new GuiButton(100, X + 10, Y + 95, 90, 20, "Valider nom"));
        this.buttonList.add(new GuiButton(101, X + 13, Y + 120, 84, 20, "Valider texture"));
        this.buttonList.add(new GuiButton(102, X + 12, Y + 170, 89, 20, "Tuer l'entité"));

        super.initGui();
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException
    {
        super.keyTyped(typedChar, keyCode);
        this.concess.textboxKeyTyped(typedChar, keyCode);
        this.texture.textboxKeyTyped(typedChar, keyCode);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException
    {
        switch(button.id)
        {
            case 100:
                CraftingLifeMod.getPacketHandler().sendToServer(new PacketDealer(concessentity.getEntityId(), 1, concess.getText()));
                playerEntity.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + TextFormatting.GREEN + "Vous avez bien modifié le nom de ce concessionaire"));
                break;
            case 101:
                if(texture.getText().equalsIgnoreCase("police"))
                {
                    concessentity.setSkin(1);
                    playerEntity.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + TextFormatting.GREEN + "Vous avez bien modifié le skin de ce concessionaire"));
                }
                else if(texture.getText().equalsIgnoreCase("pompier"))
                {
                    concessentity.setSkin(2);
                    playerEntity.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + TextFormatting.GREEN + "Vous avez bien modifié le skin de ce concessionaire"));
                }
                else if(texture.getText().equalsIgnoreCase("defaut"))
                {
                    concessentity.setSkin(3);
                    playerEntity.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + TextFormatting.GREEN + "Vous avez bien modifié le skin de ce concessionaire"));
                }
                else
                    mc.player.sendMessage(new TextComponentString("Vous pouver mettre 'police', 'pompier' ou 'defaut' comme skin !"));
                break;
            case 102:
                CraftingLifeMod.getPacketHandler().sendToServer(new PacketKillEntity(concessentity.getEntityId()));
                this.mc.player.closeScreen();
                break;
            case 103:
                this.mc.displayGuiScreen(new GuiAdminDealer(playerEntity, concessentity));
                break;
            default:
                break;

        }

        super.actionPerformed(button);
    }

    @Override
    public void updateScreen()
    {
        super.updateScreen();
        this.concess.updateCursorCounter();
        this.texture.updateCursorCounter();
    }

    public void drawScreen(int x, int y, float f)
    {
        drawDefaultBackground();

        mc.getTextureManager().bindTexture(textures);

        int X = (width - 256) / 2;
        int Y = (height - 256) / 2;
        this.drawTexturedModalRect(X, Y + 50, 0, 0, 256, 256);

        drawString(mc.fontRenderer, "Gestion du concess", X, Y + 51, 0xFFFFFF);

        this.concess.drawTextBox();
        this.texture.drawTextBox();

        super.drawScreen(x, y, f);
    }

    protected void mouseClicked(int x, int y, int btn) throws IOException
    {
        super.mouseClicked(x, y, btn);
        this.concess.mouseClicked(x, y, btn);
        this.texture.mouseClicked(x, y, btn);
    }

}
