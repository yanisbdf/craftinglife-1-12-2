package fr.yanis.craftinglifemod.client.gui;

import fr.yanis.craftinglifemod.CraftingLifeMod;
import org.lwjgl.opengl.GL11;

import com.flansmod.common.FlansMod;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;

public class GuiTypeButton extends GuiButton {

    protected static final ResourceLocation buyButton = new ResourceLocation(CraftingLifeMod.MODID,"textures/gui/Acheter.png");
    protected static final ResourceLocation closeButton = new ResourceLocation(CraftingLifeMod.MODID,"textures/gui/concessionnaire.png");
    private int type;

    public GuiTypeButton(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText, int type)
    {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
        this.type = type;
    }

	public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks)
    {
        if(!visible)
            return;

        FontRenderer fontrenderer = mc.fontRenderer;
		this.hovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
        int i = getHoverState(hovered);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        switch (type){
            case 1:
                mc.renderEngine.bindTexture(buyButton);
                drawTexturedModalRect(x, y, 0, 0, width, height);
                this.drawCenteredString(fontrenderer, "§f"+this.displayString, this.x + this.width / 2, this.y + (this.height - 8) / 2, i);
                break;
            case 2:
                mc.renderEngine.bindTexture(closeButton);
                drawTexturedModalRect(x, y, 0, 241, 23, 10);
                break;

        }

    }
    protected void mouseDragged(Minecraft p_146119_1_, int p_146119_2_, int p_146119_3_) {

    }

}
