package fr.yanis.craftinglifemod.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketKillEntity implements IMessage
{
    public int entity;

    public PacketKillEntity()
    {}

    public PacketKillEntity(int entity)
    {
        this.entity = entity;
    }

    @Override
    public void fromBytes(ByteBuf buf)
    {
        this.entity = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(entity);

    }

    public static class CommonHandler implements IMessageHandler<PacketKillEntity, IMessage>
    {
        @Override
        public IMessage onMessage(PacketKillEntity message, MessageContext ctx)
        {
            Entity entity = ctx.getServerHandler().player.world.getEntityByID(message.entity);
            entity.setDead();
            return null;
        }
    }
}
