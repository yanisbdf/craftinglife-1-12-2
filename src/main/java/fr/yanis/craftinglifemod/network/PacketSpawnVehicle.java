package fr.yanis.craftinglifemod.network;

import java.util.List;

import com.flansmod.common.driveables.DriveableData;
import com.flansmod.common.driveables.EntityDriveable;
import com.flansmod.common.driveables.EntityVehicle;
import com.flansmod.common.driveables.VehicleType;
import com.flansmod.common.parts.PartType;
import com.flansmod.common.types.EnumType;
import fr.yanis.craftinglifemod.CraftingLifeMod;
import fr.yanis.craftinglifemod.blocks.CraftingLifeBlocks;
import fr.yanis.craftinglifemod.entities.EntityDealer;
import io.netty.buffer.ByteBuf;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/*public class PacketSpawnVehicle implements IMessage
{

    private int concessId;
    private String vehicleshortname, vehicname;

    public PacketSpawnVehicle(int concessId, String vehicleshortname, String vehicname)
    {
        this.vehicleshortname = vehicleshortname;
        this.concessId = concessId;
        this.vehicname = vehicname;
    }

    public PacketSpawnVehicle()
    {}

    @Override
    public void fromBytes(ByteBuf buf)
    {
        vehicleshortname = ByteBufUtils.readUTF8String(buf);
        vehicname = ByteBufUtils.readUTF8String(buf);
        concessId = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        ByteBufUtils.writeUTF8String(buf, vehicleshortname);
        ByteBufUtils.writeUTF8String(buf, vehicname);
        buf.writeInt(concessId);
    }

    public static class CommonHandler implements IMessageHandler<PacketSpawnVehicle, IMessage>
    {
        @Override
        public IMessage onMessage(PacketSpawnVehicle message, MessageContext ctx)
        {
            EntityPlayerMP playerEntity = ctx.getServerHandler().player;
            Entity ent = playerEntity.world.getEntityByID(message.concessId);
            int price = ((EntityDealer)ent).vehicules.get(message.vehicname);
            boolean wasBlocked = false;
            for(int x = (int)(playerEntity.posX - 30); x < playerEntity.posX + 30; x++)
            {
                for(int y = (int)(playerEntity.posY - 30); y < playerEntity.posY + 30; y++)
                {
                    for(int z = (int)(playerEntity.posZ - 30); z < playerEntity.posZ + 30; z++)
                    {
                        if(playerEntity.getEntityWorld().getBlockState(new BlockPos(x, y, z)) == CraftingLifeBlocks.SPAWN)
                        {
                            IBlockState meta = playerEntity.getEntityWorld().getBlockState(new BlockPos(x, y, z));
                            System.out.println(meta);
                            if(meta == 1)
                            {
                                List<EntityDriveable> present = playerEntity.getEntityWorld().getEntitiesWithinAABB(EntityDriveable.class, AxisAlignedBB.getBoundingBox(x - 2, y - 1, z - 2, x + 2, y + 2, z + 2));
                                if(!present.isEmpty())
                                {
                                    wasBlocked = true;
                                    continue;
                                }

                                EntityVehicle vehicle;
                                NBTTagCompound tag = new NBTTagCompound();
                                tag.setString("Type", message.vehicleshortname);
                                tag.setString("Engine", ((PartType)PartType.defaultEngines.get(EnumType.vehicle)).shortName);
                                VehicleType typevehi = (VehicleType)VehicleType.getDriveable(message.vehicleshortname);
                                // FlansMod.packetHandler.sendTo(new PacketVerfication(true), playerEntity);
                                if(typevehi != null)
                                {
                                    if(price > 0)
                                    {
                                        if(econ.getInventoryCash(playerEntity) >= price)
                                        {
                                            double paidAmount = econ.findCashInInventoryWithChange(playerEntity, price);
                                            if(paidAmount > 0)
                                                econ.giveChange(paidAmount, price, playerEntity);
                                        }
                                        else
                                        {
                                            playerEntity.addChatComponentMessage(new ChatComponentText(ModAW.PREFIX + EnumChatFormatting.RED + "Vous n'avez pas assez d'argent sur vous !"));
                                            return null;
                                        }
                                    }
                                    vehicle = new EntityVehicle(playerEntity.getEntityWorld(), x, y + 2, z, typevehi, new DriveableData(tag));
                                    vehicle.setOwner(playerEntity.getGameProfile());
                                    playerEntity.getEntityWorld().spawnEntityInWorld(vehicle);
                                    playerEntity.addChatComponentMessage(new ChatComponentText(ModAW.PREFIX + EnumChatFormatting.GREEN + "Payement effectué avec succès."));
                                    playerEntity.addChatComponentMessage(new ChatComponentText(ModAW.PREFIX + EnumChatFormatting.GREEN + "Un voiturier a garé votre véhicule sur le parking avant §ade le vérrouillé."));
                                }
                                return null;
                            }
                        }
                    }
                }
            }
            if(wasBlocked)
                playerEntity.addChatComponentMessage(new ChatComponentText(ModAW.PREFIX + EnumChatFormatting.RED + "Toute les places de parking sont prises !"));
            else
                playerEntity.addChatComponentMessage(new ChatComponentText(ModAW.PREFIX + EnumChatFormatting.RED + "Il n'y a pas de parking dans les environ. !"));
            return null;
        }
    }
    

}*/
