package fr.yanis.craftinglifemod.network;

import com.flansmod.common.driveables.EntityVehicle;
import com.flansmod.common.network.PacketPlaySound;
import fr.yanis.craftinglifemod.CraftingLifeMod;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

import java.util.List;

public class PacketVehicleLock implements IMessage {


	public PacketVehicleLock() {
	}

	@Override
	public void fromBytes(ByteBuf buf) {
	}

	@Override
	public void toBytes(ByteBuf buf) {
	}

	public static class CommonHandler implements IMessageHandler<PacketVehicleLock, IMessage> {
		@Override
		public IMessage onMessage(PacketVehicleLock message, MessageContext ctx) {
			EntityPlayerMP entityPlayerMP = ctx.getServerHandler().player;
			List vehicles = entityPlayerMP.world.getEntitiesWithinAABB(EntityVehicle.class, entityPlayerMP.getEntityBoundingBox().grow(5D, 5D, 5D));
			if (vehicles.size() == 0) {
				entityPlayerMP.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + TextFormatting.RED + "Vous étes trop loin de votre véhicule."));
			}
			for (Object o : vehicles) {
				if (o instanceof EntityVehicle) {
					EntityVehicle vehicle = (EntityVehicle) o;
					if (vehicle.getOwner().getId().equals(entityPlayerMP.getGameProfile().getId())) {
						if (vehicle.isVehicleLocked()) {
							vehicle.setVehicleLocked(false);
							entityPlayerMP.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + TextFormatting.GREEN + "Votre véhicule est déverrouillé."));
							PacketPlaySound.sendSoundPacket(vehicle.posX, vehicle.posY, vehicle.posZ, 10D, vehicle.dimension, "unlock", false);
						} else {
							vehicle.setVehicleLocked(true);
							entityPlayerMP.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + TextFormatting.RED + "Votre véhicule est vérrouillé."));
							PacketPlaySound.sendSoundPacket(vehicle.posX, vehicle.posY, vehicle.posZ, 10D, vehicle.dimension, "lock", false);
						}
					} else {
						entityPlayerMP.sendMessage(new TextComponentString(CraftingLifeMod.PREFIX + TextFormatting.RED + "Ce n'est pas votre véhicule."));
					}
				}
			}

			return null;
		}
	}
}


