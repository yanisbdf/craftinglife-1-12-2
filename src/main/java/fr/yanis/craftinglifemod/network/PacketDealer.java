package fr.yanis.craftinglifemod.network;

import fr.yanis.craftinglifemod.entities.EntityDealer;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketDealer implements IMessage
{
    private int action;
    private String name;
    private int concessEntity;

    public PacketDealer() {}

    public PacketDealer(int concessEntity, int action, String name)
    {
        this.concessEntity = concessEntity;
        this.action = action;
        this.name = name;
    }

    @Override
    public void fromBytes(ByteBuf buf)
    {
        this.concessEntity = buf.readInt();
        this.action = buf.readInt();
        this.name = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(concessEntity);
        buf.writeInt(action);
        ByteBufUtils.writeUTF8String(buf, name);
    }

    public static class CommonHandler implements IMessageHandler<PacketDealer, IMessage>
    {
        @Override
        public IMessage onMessage(PacketDealer message, MessageContext ctx)
        {
            Entity concessEntity = ctx.getServerHandler().player.world.getEntityByID(message.concessEntity);
            if(concessEntity instanceof EntityDealer)
            {
                EntityDealer concess = (EntityDealer)concessEntity;
                if(message.action == 1)
                    concess.concessName = message.name;
            }
            return null;
        }
    }
}
