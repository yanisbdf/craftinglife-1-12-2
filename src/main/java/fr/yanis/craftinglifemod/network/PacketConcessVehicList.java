package fr.yanis.craftinglifemod.network;

import fr.yanis.craftinglifemod.entities.EntityDealer;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketConcessVehicList implements IMessage
{
    public static class HandlerServer implements IMessageHandler<PacketConcessVehicList, IMessage>
    {
        @Override
        public IMessage onMessage(PacketConcessVehicList message, MessageContext ctx) {
            int action = message.data.getInteger("Action");
            if(action == 1) { //Add
                Entity concess = ctx.getServerHandler().player.world.getEntityByID(message.data.getInteger("EntId"));
                if(concess instanceof EntityDealer) {
                    ((EntityDealer) concess).vehicules.put(message.data.getString("nam"), message.data.getInteger("price"));
                }
            }
            else if(action == 2) { //Remove
                Entity concess = ctx.getServerHandler().player.world.getEntityByID(message.data.getInteger("EntId"));
                if(concess instanceof EntityDealer) {
                    ((EntityDealer) concess).vehicules.remove(message.data.getString("namr"));
                }
            }
            return null;
        }
    }
    public static class HandlerClient implements IMessageHandler<PacketConcessVehicList, IMessage>
    {
        @Override
        @SideOnly(Side.CLIENT)
        public IMessage onMessage(PacketConcessVehicList message, MessageContext ctx) {
            int action = message.data.getInteger("Action");
            /*if(action == 1) { //Add
                int list = message.data.getInteger("List");
                switch(list) {
                case 0:
                    WorldSaveData.vehicivil.put(message.data.getString("vehic"), message.data.getInteger("price"));
                    break;
                case 1:
                    WorldSaveData.vehigendarmerie.put(message.data.getString("vehic"), message.data.getInteger("price"));
                    break;
                case 2:
                    WorldSaveData.vehiluxe.put(message.data.getString("vehic"), message.data.getInteger("price"));
                    break;
                case 3:
                    WorldSaveData.vehicamion.put(message.data.getString("vehic"), message.data.getInteger("price"));
                    break;
                case 4:
                    WorldSaveData.vehipompier.put(message.data.getString("vehic"), message.data.getInteger("price"));
                    break;
                }
            }
            else */if(action == 2) { //Set all and show gui
                Entity concess = Minecraft.getMinecraft().world.getEntityByID(message.data.getInteger("EntId"));
                if(concess instanceof EntityDealer) {
                    ((EntityDealer) concess).readVehiculesDataFromNBT(message.data);
                    ((EntityDealer) concess).guiDisplayConcessionnaire(Minecraft.getMinecraft().player);
                }
            }
            else if(action == 3) { //Set all and show setup gui
                Entity concess = Minecraft.getMinecraft().world.getEntityByID(message.data.getInteger("EntId"));
                if(concess instanceof EntityDealer) {
                    ((EntityDealer) concess).readVehiculesDataFromNBT(message.data);
                    ((EntityDealer) concess).guiDisplaySetup(Minecraft.getMinecraft().player);
                }

            }
            return null;
        }
    }

    private NBTTagCompound data;
    
    public PacketConcessVehicList() {}
    public PacketConcessVehicList(NBTTagCompound data) {
        this.data = data;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        data = ByteBufUtils.readTag(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeTag(buf, data);
    }

}
