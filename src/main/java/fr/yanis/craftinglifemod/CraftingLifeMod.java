package fr.yanis.craftinglifemod;

import com.google.gson.Gson;
import fr.yanis.craftinglifemod.blocks.CraftingLifeBlocks;
import fr.yanis.craftinglifemod.blocks.tileentities.*;
import fr.yanis.craftinglifemod.client.ClientEventHandler;
import fr.yanis.craftinglifemod.common.CommonProxy;
import fr.yanis.craftinglifemod.handlers.RegisteringHandler;
import fr.yanis.craftinglifemod.items.CraftingLifeItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.lwjgl.opengl.Display;

@Mod(modid = CraftingLifeMod.MODID, name = CraftingLifeMod.NAME, version = CraftingLifeMod.VERSION)
public class CraftingLifeMod {

	public static final String PREFIX = TextFormatting.GOLD + "[" + TextFormatting.AQUA + "CraftingLife" + TextFormatting.GOLD + "]";
	public static final String MODID = "craftinglifemod";
	public static final String NAME = "CraftingLifeMod";
	public static final String VERSION = "5.0.0";

	@Mod.Instance(CraftingLifeMod.MODID)
	private static CraftingLifeMod instance;

	@SidedProxy(clientSide = "fr.yanis.craftinglifemod.client.ClientProxy", serverSide = "fr.yanis.craftinglifemod.common.CommonProxy")
	public static CommonProxy proxy;

	private static SimpleNetworkWrapper network = NetworkRegistry.INSTANCE.newSimpleChannel(CraftingLifeMod.MODID);

	public static CreativeTabs craftingLifeModBlocksCreativeTabs = new CreativeTabs("craftingmodblocks_creative_tabs") {
		@Override
		public ItemStack createIcon() {
			return new ItemStack(Item.getItemFromBlock(CraftingLifeBlocks.REDBERRYBUSH));
		}
	};

	public static CreativeTabs craftingLifeModItemsCreativeTabs = new CreativeTabs("craftingmoditems_creative_tabs") {
		@Override
		public ItemStack createIcon() {
			return new ItemStack(CraftingLifeItems.WRENCH);
		}
	};

	public static CreativeTabs craftingLifeModFoodCreativeTabs = new CreativeTabs("craftingmodfood_creative_tabs") {
		@Override
		public ItemStack createIcon() {
			return new ItemStack(CraftingLifeItems.MOUNTAINDEW);
		}
	};

	public static Gson gsonInstance = new Gson();


	public CraftingLifeMod(){
		MinecraftForge.EVENT_BUS.register(new RegisteringHandler());
		if(FMLCommonHandler.instance().getSide().isClient())
		{
			Display.setTitle("CraftingLife V3 | Minecraft 1.12.2");
		}
	}
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event){
		//Registering packets
		RegisteringHandler.registerMessages();

		//Registering Entities Renders
		proxy.registerEntitiesRenders();
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event){
		proxy.registerRenders();
		proxy.init();

		//Registering tile entitites
		GameRegistry.registerTileEntity(TEFlag.class,  new ResourceLocation(CraftingLifeMod.MODID + "storecashregister"));
		GameRegistry.registerTileEntity(TECocaDispenser.class, new ResourceLocation(CraftingLifeMod.MODID + "cocadispenser"));
		GameRegistry.registerTileEntity(TEDeskD.class, new ResourceLocation(CraftingLifeMod.MODID + "deskd"));
		GameRegistry.registerTileEntity(TEDisplay.class, new ResourceLocation(CraftingLifeMod.MODID + "display"));
		GameRegistry.registerTileEntity(TEDrillMachine.class, new ResourceLocation(CraftingLifeMod.MODID + "drillmachine"));
		GameRegistry.registerTileEntity(TEElectricityMeter.class, new ResourceLocation(CraftingLifeMod.MODID + "electricitymeter"));
		GameRegistry.registerTileEntity(TEFence.class, new ResourceLocation(CraftingLifeMod.MODID + "fence"));
		GameRegistry.registerTileEntity(TEFireExtinguisher.class, new ResourceLocation(CraftingLifeMod.MODID + "fireenxtinguisher"));
		GameRegistry.registerTileEntity(TEFuelPump.class, new ResourceLocation(CraftingLifeMod.MODID + "fuelpumo"));
		GameRegistry.registerTileEntity(TEFoodStand.class, new ResourceLocation(CraftingLifeMod.MODID + "foodstand"));
		GameRegistry.registerTileEntity(TEFootBallCage.class, new ResourceLocation(CraftingLifeMod.MODID + "footballcage"));
		GameRegistry.registerTileEntity(TEGarage.class, new ResourceLocation(CraftingLifeMod.MODID + "garage"));
		GameRegistry.registerTileEntity(TEGarbageBin.class, new ResourceLocation(CraftingLifeMod.MODID + "garagebin"));
		GameRegistry.registerTileEntity(TELockers.class, new ResourceLocation(CraftingLifeMod.MODID + "lockers"));
		GameRegistry.registerTileEntity(TEGrapePress.class, new ResourceLocation(CraftingLifeMod.MODID + "grpepress"));
		GameRegistry.registerTileEntity(TEOrangeBerryBush.class, new ResourceLocation(CraftingLifeMod.MODID + "orangeberrybush"));
		GameRegistry.registerTileEntity(TEAtm.class, new ResourceLocation(CraftingLifeMod.MODID + "phonef"));
		GameRegistry.registerTileEntity(TEPlainware.class, new ResourceLocation(CraftingLifeMod.MODID + "plainware"));
		GameRegistry.registerTileEntity(TERedBerryBush.class, new ResourceLocation(CraftingLifeMod.MODID + "redberrybush"));
		GameRegistry.registerTileEntity(TESpawnB.class, new ResourceLocation(CraftingLifeMod.MODID + "spawnb"));
		GameRegistry.registerTileEntity(TESwitch.class, new ResourceLocation(CraftingLifeMod.MODID + "switch"));
		GameRegistry.registerTileEntity(TECameraB.class, new ResourceLocation(CraftingLifeMod.MODID + "camerab"));
		GameRegistry.registerTileEntity(TETrafficLight.class, new ResourceLocation(CraftingLifeMod.MODID + "trafficlight"));
		GameRegistry.registerTileEntity(TEVentilation.class, new ResourceLocation(CraftingLifeMod.MODID + "ventilation"));

		if(event.getSide().isClient()) {
			MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
		}
	}

	public static CraftingLifeMod getInstance(){
		return instance;
	}

	public static SimpleNetworkWrapper getPacketHandler() {
		return network;
	}
}
